package crucisaid.logic;

import crucisaid.model.Bloqueos;
import crucisaid.model.CasillaDto;
import crucisaid.model.Condicion;
import crucisaid.model.Imagen;
import crucisaid.model.Leyenda;
import crucisaid.model.Palabra;
import crucisaid.model.Posicion;
import crucisaid.model.Simbolo;
import crucisaid.utils.Constantes;
import crucisaid.utils.DatabaseAccess;
import crucisaid.utils.Modelos;
import crucisaid.utils.Parametros;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Carlos
 */
public class CruciUtils {

    /**
     * Este metodo no contempla si la posicion esta ocupada o no, tiene la
     * finalidad de encontrar la ruta por donde ira colocando la palabra de una
     * imagen, dependiendo del modelo elegido.
     *
     * @param posicion X Y DIRECCION de la posicion actual
     * @param MAX_X Es el limite horizontal del crucigrama
     * @param MAX_Y Es el limite vertical del crucigrama
     * @return la nueva posicion donde se colocara la letra.
     */
    public static Posicion posicion(Posicion posicion, int MAX_X, int MAX_Y, int ceroX, int ceroY) {
        int x = posicion.getX();
        int y = posicion.getY();
        int direccion = posicion.getDireccion();

        switch (direccion) {
            case Constantes.DIRECCION_ABAJO: {
                if (y + 1 < MAX_Y) {
                    y = y + 1;
                } else {
                    x = x + 1;
                    direccion = Constantes.DIRECCION_DERECHA;
                }
                break;
            }
            case Constantes.DIRECCION_DERECHA: {
                if (x + 1 < MAX_X) {
                    x = x + 1;
                } else {
                    y = y - 1;
                    direccion = Constantes.DIRECCION_ARRIBA;
                }
                break;
            }
            case Constantes.DIRECCION_ARRIBA: {
                if (y - 1 >= ceroY) {
                    y = y - 1;
                } else {
                    x = x - 1;
                    direccion = Constantes.DIRECCION_IZQUIERDA;
                }
                break;
            }
            case Constantes.DIRECCION_IZQUIERDA: {
                if (x - 1 >= ceroX) {
                    x = x - 1;
                } else {
                    y = y + 1;
                    direccion = Constantes.DIRECCION_ABAJO;
                }
                break;
            }
        }

        Posicion p = new Posicion();
        p.setX(x);
        p.setY(y);
        p.setDireccion(direccion);
        return p;
    }

    public static List<Posicion> obtenerRuta(Posicion posicion, String ruta) {
        List<Posicion> rutaMapa = new ArrayList<Posicion>();
        String[] partes = ruta.split("-");
        int x = posicion.getX();
        int y = posicion.getY();
        rutaMapa.add(posicion);
        for (int i = 0; i < partes.length; i++) {
            int direccion = Constantes.DIRECCION_DERECHA;
            int masX = 0;
            int masY = 0;
            if (partes[i].startsWith("A")) {
                direccion = Constantes.DIRECCION_ABAJO;
                masY = 1;
            }
            if (partes[i].startsWith("R")) {
                direccion = Constantes.DIRECCION_ARRIBA;
                masY = -1;
            }
            if (partes[i].startsWith("I")) {
                direccion = Constantes.DIRECCION_IZQUIERDA;
                masX = -1;
            }
            if (partes[i].startsWith("D")) {
                direccion = Constantes.DIRECCION_DERECHA;
                masX = 1;
            }
            int cantidad = Integer.parseInt(partes[i].substring(1));
            for (int j = 0; j < cantidad; j++) {
                y = y + masY;
                x = x + masX;
                rutaMapa.add(new Posicion(x, y, direccion));
            }
        }
        return rutaMapa;
    }

    public static Posicion posicionGenerica(Posicion posicion, int actual, String ruta) {
        int x = posicion.getX();
        int y = posicion.getY();

        int direccion = posicion.getDireccion();
        Posicion p = new Posicion();
        p.setX(x);
        p.setY(y);
        p.setDireccion(direccion);
        return p;
    }

    public static int direccionInicial(String instruccion) {
        if (instruccion.startsWith("A")) {
            return Constantes.DIRECCION_ABAJO;
        }
        if (instruccion.startsWith("R")) {
            return Constantes.DIRECCION_ARRIBA;
        }
        if (instruccion.startsWith("I")) {
            return Constantes.DIRECCION_IZQUIERDA;
        }
        return Constantes.DIRECCION_DERECHA; //default direction
    }

    static Simbolo simbolo(Posicion anterior, Posicion actual, boolean ultimo, boolean primero, CasillaDto casilla) {
        Simbolo simbolo = new Simbolo();
        switch (anterior.getDireccion()) {
            case Constantes.DIRECCION_ABAJO: {
                if (ultimo) {
                    casilla.setFinalAbajo(true);
                    simbolo.setIdSimbolo(Constantes.SIMBOLO_FINAL);
                    simbolo.setPosicionDibujar(Constantes.DIBUJAR_ABAJO);
                } else {
                    if (primero) {
                        casilla.setInicioAbajo(true);
                        simbolo.setIdSimbolo(Constantes.SIMBOLO_INICIAL);
                        simbolo.setPosicionDibujar(Constantes.DIBUJAR_ARRIBA);
                    } else {
                        if (actual.getX() > anterior.getX()) {
                            casilla.setCambioAbajoIzquierda(true);
                            // HA ROTADO HACIA LA DERECHA
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ABAJ_IZQ);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ABAJ_IZQ);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        } else {
                            casilla.setCambioAbajoDerecha(true);
                            // HA ROTADO HACIA LA IZQUIERDA
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ABAJ_DER);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ABAJ_DER);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        }
                    }
                }
                break;
            }
            case Constantes.DIRECCION_DERECHA: {
                if (ultimo) {
                    casilla.setFinalDerecha(true);
                    simbolo.setIdSimbolo(Constantes.SIMBOLO_FINAL);
                    simbolo.setPosicionDibujar(Constantes.DIBUJAR_DERECHA);
                } else {
                    if (primero) {
                        casilla.setInicioDerecha(true);
                        simbolo.setIdSimbolo(Constantes.SIMBOLO_INICIAL);
                        simbolo.setPosicionDibujar(Constantes.DIBUJAR_IZQUIERDA);
                    } else {
                        if (actual.getY() > anterior.getY()) {
                            casilla.setCambioArribaDerecha(true);
                            // HA ROTADO HACIA ABAJO
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ARRI_DER);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ARRI_DER);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        } else {
                            casilla.setCambioAbajoDerecha(true);
                            // HA ROTADO HACIA ARRIBA
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ABAJ_DER);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ABAJ_DER);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        }
                    }
                }
                break;
            }
            case Constantes.DIRECCION_ARRIBA: {
                if (ultimo) {
                    casilla.setFinalArriba(true);
                    simbolo.setIdSimbolo(Constantes.SIMBOLO_FINAL);
                    simbolo.setPosicionDibujar(Constantes.DIBUJAR_ARRIBA);
                } else {
                    if (primero) {
                        casilla.setInicioArriba(true);
                        simbolo.setIdSimbolo(Constantes.SIMBOLO_INICIAL);
                        simbolo.setPosicionDibujar(Constantes.DIBUJAR_ABAJO);
                    } else {
                        if (actual.getX() > anterior.getX()) {
                            casilla.setCambioArribaDerecha(true);
                            // HA ROTADO HACIA LA DERECHA
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ARRI_IZQ);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ARRI_IZQ);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        } else {
                            casilla.setCambioArribaDerecha(true);
                            // HA ROTADO HACIA LA IZQUIERDA
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ARRI_DER);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ARRI_DER);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        }
                    }
                }
                break;
            }
            case Constantes.DIRECCION_IZQUIERDA: {
                if (ultimo) {
                    casilla.setFinalIzquierda(true);
                    simbolo.setIdSimbolo(Constantes.SIMBOLO_FINAL);
                    simbolo.setPosicionDibujar(Constantes.DIBUJAR_IZQUIERDA);
                } else {
                    if (primero) {
                        casilla.setInicioIzquierda(true);
                        simbolo.setIdSimbolo(Constantes.SIMBOLO_INICIAL);
                        simbolo.setPosicionDibujar(Constantes.DIBUJAR_DERECHA);
                    } else {
                        if (actual.getY() > anterior.getY()) {
                            casilla.setCambioArribaIzquierda(true);
                            // HA ROTADO HACIA ABAJO
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ARRI_IZQ);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ARRI_IZQ);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        } else {
                            casilla.setCambioAbajoIzquierda(true);
                            // HA ROTADO HACIA ARRIBA
                            simbolo.setIdSimbolo(Constantes.SIMBOLO_ABAJ_IZQ);
                            simbolo.setPosicionDibujar(Constantes.DIBUJAR_ABAJ_IZQ);/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                        }
                    }
                }
                break;
            }
        }
        return simbolo;
    }

    static Simbolo simboloEspacio(Posicion posicion) {
        Simbolo simbolo = new Simbolo();
        simbolo.setIdSimbolo(Constantes.SIMBOLO_ESPACIO);
        switch (posicion.getDireccion()) {
            case Constantes.DIRECCION_ABAJO: {
                simbolo.setPosicionDibujar(Constantes.DIBUJAR_ARRIBA);
                break;
            }
            case Constantes.DIRECCION_DERECHA: {
                simbolo.setPosicionDibujar(Constantes.DIBUJAR_IZQUIERDA);
                break;
            }
            case Constantes.DIRECCION_ARRIBA: {
                simbolo.setPosicionDibujar(Constantes.DIBUJAR_ABAJO);
                break;
            }
            case Constantes.DIRECCION_IZQUIERDA: {
                simbolo.setPosicionDibujar(Constantes.DIBUJAR_DERECHA);
                break;
            }
        }
        return simbolo;
    }

    static Leyenda guia(Posicion posicion, Palabra palabra, CasillaDto casilla) {
        Leyenda leyenda = new Leyenda();
        switch (posicion.getDireccion()) {
            case Constantes.DIRECCION_ABAJO: {
                casilla.setAyudaAbajo(palabra.getDescripcion());
                leyenda.setLeyenda(palabra);
                leyenda.setPosicionDibujar(Constantes.DIBUJAR_ARRIBA);
                break;
            }
            case Constantes.DIRECCION_DERECHA: {
                casilla.setAyudaDerecha(palabra.getDescripcion());
                leyenda.setLeyenda(palabra);
                leyenda.setPosicionDibujar(Constantes.DIBUJAR_IZQUIERDA);
                break;
            }
            case Constantes.DIRECCION_ARRIBA: {
                casilla.setAyudaArriba(palabra.getDescripcion());
                leyenda.setLeyenda(palabra);
                leyenda.setPosicionDibujar(Constantes.DIBUJAR_ABAJO);
                break;
            }
            case Constantes.DIRECCION_IZQUIERDA: {
                casilla.setAyudaIzquierda(palabra.getDescripcion());
                leyenda.setLeyenda(palabra);
                leyenda.setPosicionDibujar(Constantes.DIBUJAR_DERECHA);
                break;
            }
        }
        return leyenda;
    }

    static Palabra pedirPalabra(Component component, int maxLongitud) {
        Palabra palabra = new Palabra();
        palabra.setId(0);
        String text = "";
        String mensaje = "PALABRA PARA LA IMAGEN (Max. " + maxLongitud + " letras)";
        JTextArea ta = new JTextArea(3, 20);
        ta.setLineWrap(true);
        while (text.replace(" ", "").length() < 3 || text.replace(" ", "").length() > maxLongitud) {
            switch (JOptionPane.showConfirmDialog(component, new JScrollPane(ta), mensaje, JOptionPane.OK_CANCEL_OPTION)) {
                case JOptionPane.OK_OPTION: {
                    text = ta.getText();
                    text = text.replace("\r\n", " ");
                    text = text.replace("\r", " ");
                    text = text.replace("\n", " ");
                    text = text.replace("  ", " ");
                    text = text.replace("  ", " ");
                    text = text.replace("  ", " ");
                    break;
                }
                case JOptionPane.CANCEL_OPTION: {
                    return null;
                }
            }
            if (text.length() < 3) {
                mensaje = "Palabra no válida (Min. 3 letras) ";
            }
            if (text.length() > maxLongitud) {
                mensaje = "Palabra no válida (Max. " + maxLongitud + " letras)";
            }
        }
        palabra.setDescripcion("");
        palabra.setPalabra(text.toUpperCase());
        palabra.setCantidad(text.length());
        return palabra;
    }

    private static String[][] returnCrucigrama() {
        String[][] crucigrama = new String[Modelos.MODELO1.length][Modelos.MODELO1[0].length];
        for (int i = 0; i < Modelos.MODELO1.length; i++) {
            for (int j = 0; j < Modelos.MODELO1[0].length; j++) {
                crucigrama[i][j] = Modelos.MODELO1[i][j];
                System.out.print(crucigrama[i][j]);
            }
            System.out.println();
        }
        System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
        return crucigrama;

    }

    public static String[][] inicializar(int plantilla) {
        switch (plantilla) {
            case Constantes.MODELO_1: {
                String[][] crucigrama = new String[Modelos.MODELO1.length][Modelos.MODELO1[0].length];
                for (int i = 0; i < Modelos.MODELO1.length; i++) {
                    for (int j = 0; j < Modelos.MODELO1[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO1[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_2: {
                String[][] crucigrama = new String[Modelos.MODELO2.length][Modelos.MODELO2[0].length];
                for (int i = 0; i < Modelos.MODELO2.length; i++) {
                    for (int j = 0; j < Modelos.MODELO2[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO2[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_3: {
                String[][] crucigrama = new String[Modelos.MODELO3.length][Modelos.MODELO3[0].length];
                for (int i = 0; i < Modelos.MODELO3.length; i++) {
                    for (int j = 0; j < Modelos.MODELO3[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO3[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_4: {
                String[][] crucigrama = new String[Modelos.MODELO4.length][Modelos.MODELO4[0].length];
                for (int i = 0; i < Modelos.MODELO4.length; i++) {
                    for (int j = 0; j < Modelos.MODELO4[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO4[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_5: {
                String[][] crucigrama = new String[Modelos.MODELO5.length][Modelos.MODELO5[0].length];
                for (int i = 0; i < Modelos.MODELO5.length; i++) {
                    for (int j = 0; j < Modelos.MODELO5[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO5[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_6: {
                String[][] crucigrama = new String[Modelos.MODELO6.length][Modelos.MODELO6[0].length];
                for (int i = 0; i < Modelos.MODELO6.length; i++) {
                    for (int j = 0; j < Modelos.MODELO6[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO6[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_7: {
                String[][] crucigrama = new String[Modelos.MODELO7.length][Modelos.MODELO7[0].length];
                for (int i = 0; i < Modelos.MODELO7.length; i++) {
                    for (int j = 0; j < Modelos.MODELO7[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO7[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_8: {
                String[][] crucigrama = new String[Modelos.MODELO8.length][Modelos.MODELO8[0].length];
                for (int i = 0; i < Modelos.MODELO8.length; i++) {
                    for (int j = 0; j < Modelos.MODELO8[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO8[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_9: {
                String[][] crucigrama = new String[Modelos.MODELO9.length][Modelos.MODELO9[0].length];
                for (int i = 0; i < Modelos.MODELO9.length; i++) {
                    for (int j = 0; j < Modelos.MODELO9[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO9[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_10: {
                String[][] crucigrama = new String[Modelos.MODELO10.length][Modelos.MODELO10[0].length];
                for (int i = 0; i < Modelos.MODELO10.length; i++) {
                    for (int j = 0; j < Modelos.MODELO10[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO10[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_11: {
                String[][] crucigrama = new String[Modelos.MODELO11.length][Modelos.MODELO11[0].length];
                for (int i = 0; i < Modelos.MODELO11.length; i++) {
                    for (int j = 0; j < Modelos.MODELO11[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO11[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_12: {
                String[][] crucigrama = new String[Modelos.MODELO12.length][Modelos.MODELO12[0].length];
                for (int i = 0; i < Modelos.MODELO12.length; i++) {
                    for (int j = 0; j < Modelos.MODELO12[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO12[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_13: {
                String[][] crucigrama = new String[Modelos.MODELO13.length][Modelos.MODELO13[0].length];
                for (int i = 0; i < Modelos.MODELO13.length; i++) {
                    for (int j = 0; j < Modelos.MODELO13[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO13[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_14: {
                String[][] crucigrama = new String[Modelos.MODELO14.length][Modelos.MODELO14[0].length];
                for (int i = 0; i < Modelos.MODELO14.length; i++) {
                    for (int j = 0; j < Modelos.MODELO14[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO14[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_15: {
                String[][] crucigrama = new String[Modelos.MODELO15.length][Modelos.MODELO15[0].length];
                for (int i = 0; i < Modelos.MODELO15.length; i++) {
                    for (int j = 0; j < Modelos.MODELO15[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO15[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_16: {
                String[][] crucigrama = new String[Modelos.MODELO16.length][Modelos.MODELO16[0].length];
                for (int i = 0; i < Modelos.MODELO16.length; i++) {
                    for (int j = 0; j < Modelos.MODELO16[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO16[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_17: {
                String[][] crucigrama = new String[Modelos.MODELO17.length][Modelos.MODELO17[0].length];
                for (int i = 0; i < Modelos.MODELO17.length; i++) {
                    for (int j = 0; j < Modelos.MODELO17[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO17[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_18: {
                String[][] crucigrama = new String[Modelos.MODELO18.length][Modelos.MODELO18[0].length];
                for (int i = 0; i < Modelos.MODELO18.length; i++) {
                    for (int j = 0; j < Modelos.MODELO18[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO18[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_19: {
                String[][] crucigrama = new String[Modelos.MODELO19.length][Modelos.MODELO19[0].length];
                for (int i = 0; i < Modelos.MODELO19.length; i++) {
                    for (int j = 0; j < Modelos.MODELO19[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO19[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
            case Constantes.MODELO_20: {
                String[][] crucigrama = new String[Modelos.MODELO20.length][Modelos.MODELO20[0].length];
                for (int i = 0; i < Modelos.MODELO20.length; i++) {
                    for (int j = 0; j < Modelos.MODELO20[0].length; j++) {
                        crucigrama[i][j] = Modelos.MODELO20[i][j];
                        System.out.print(crucigrama[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Crucigrama cargado: " + crucigrama.length + "-" + crucigrama[0].length);
                return crucigrama;
            }
        }
        return new String[0][0];
    }

    public static void cargarBD(String archivoPalabras) throws Exception {
        FileReader fr = new FileReader(archivoPalabras);
        BufferedReader text = new BufferedReader(fr);
        String line;
        Connection conn = DatabaseAccess.abrir();
        DatabaseAccess.destruir(conn);
        DatabaseAccess.crear(conn);
        while ((line = text.readLine()) != null) {
            String[] linea = line.split(",");
            int _longitud = Integer.parseInt(linea[0]);
            String _palabra = linea[1];
            String _descripcion = linea[2];
            Palabra palabra = new Palabra(_longitud, _palabra, _descripcion);
            DatabaseAccess.push(conn, palabra);
            System.out.println("Adicionada: " + palabra.toString());
        }
        text.close();
        DatabaseAccess.cerrar(conn);
    }

    public static Bloqueos bloqueoIntermedio(Posicion posicion, Posicion nueva) {
        Bloqueos resp = new Bloqueos();
        resp.setBloqueoArriba(posicion.getDireccion() == Constantes.DIRECCION_ABAJO
                || posicion.getDireccion() == Constantes.DIRECCION_ARRIBA
                || nueva.getDireccion() == Constantes.DIRECCION_ABAJO
                || nueva.getDireccion() == Constantes.DIRECCION_ARRIBA);
        resp.setBloqueoAbajo(posicion.getDireccion() == Constantes.DIRECCION_ABAJO
                || posicion.getDireccion() == Constantes.DIRECCION_ARRIBA
                || nueva.getDireccion() == Constantes.DIRECCION_ABAJO
                || nueva.getDireccion() == Constantes.DIRECCION_ARRIBA);
        resp.setBloqueoIzquierda(posicion.getDireccion() == Constantes.DIRECCION_IZQUIERDA
                || posicion.getDireccion() == Constantes.DIRECCION_DERECHA
                || nueva.getDireccion() == Constantes.DIRECCION_IZQUIERDA
                || nueva.getDireccion() == Constantes.DIRECCION_DERECHA);
        resp.setBloqueoDerecha(posicion.getDireccion() == Constantes.DIRECCION_IZQUIERDA
                || posicion.getDireccion() == Constantes.DIRECCION_DERECHA
                || nueva.getDireccion() == Constantes.DIRECCION_IZQUIERDA
                || nueva.getDireccion() == Constantes.DIRECCION_DERECHA);
        return resp;
    }

    public static Bloqueos bloqueoInicial(Posicion posicion) {
        Bloqueos resp = new Bloqueos();
        resp.setBloqueoArriba(
                posicion.getDireccion() == Constantes.DIRECCION_ABAJO
                || posicion.getDireccion() == Constantes.DIRECCION_ARRIBA);
        resp.setBloqueoAbajo(
                posicion.getDireccion() == Constantes.DIRECCION_ABAJO
                || posicion.getDireccion() == Constantes.DIRECCION_ARRIBA);
        resp.setBloqueoIzquierda(
                posicion.getDireccion() == Constantes.DIRECCION_DERECHA
                || posicion.getDireccion() == Constantes.DIRECCION_IZQUIERDA);
        resp.setBloqueoDerecha(
                posicion.getDireccion() == Constantes.DIRECCION_DERECHA
                || posicion.getDireccion() == Constantes.DIRECCION_IZQUIERDA);
        return resp;
    }

    public static Imagen obtenerImagen(int indice, String[] inicios, String[] imagenes) {
        Imagen imagen = new Imagen();
        int x = Integer.parseInt(inicios[indice].split("-")[0]);
        int y = Integer.parseInt(inicios[indice].split("-")[1]);

        int ancho = Integer.parseInt(imagenes[indice].split("-")[0]);
        int alto = Integer.parseInt(imagenes[indice].split("-")[1]);

        imagen.setX(x);
        imagen.setY(y);
        imagen.setAncho(ancho);
        imagen.setAlto(alto);
        return imagen;
    }

    public static Palabra buscar(Condicion condicion) {
        Palabra p = null;
        try {
            Connection conn = DatabaseAccess.abrir();
            p = DatabaseAccess.pop(conn, condicion);
            DatabaseAccess.cerrar(conn);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return p;
    }

    public static void limpiar() {
        try {
            Connection conn = DatabaseAccess.abrir();
            DatabaseAccess.limpiar(conn);
            DatabaseAccess.cerrar(conn);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
