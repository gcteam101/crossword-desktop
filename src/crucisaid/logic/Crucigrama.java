package crucisaid.logic;

import crucisaid.Principal;
import crucisaid.model.Bloqueos;
import crucisaid.model.CasillaDto;
import crucisaid.model.Condicion;
import crucisaid.model.Imagen;
import crucisaid.model.Leyenda;
import crucisaid.model.Palabra;
import crucisaid.model.Posicion;
import crucisaid.model.Simbolo;
import crucisaid.utils.Constantes;
import crucisaid.utils.DatabaseAccess;
import crucisaid.utils.Parametros;
import java.awt.Component;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class Crucigrama {

    private final static Logger log = Logger.getLogger(Crucigrama.class);

    //ANCHO Y ALTO DEL CRUCIGRAMA
    private final int MAX_X = Parametros.matrixX;
    private final int MAX_Y = Parametros.matrixY;

    private final Principal principal;
    private final Component component;
    private final Map<Posicion, Bloqueos> bloqueos;
    private final String[][] crucigrama;
    private final CasillaDto[][] crucigramaPaint;
    private final Map<Posicion, List<Simbolo>> simbolos;
    private final Map<Posicion, List<Leyenda>> guias;
    private final int plantilla;

    public Crucigrama(Principal principal, Component component, 
            int plantilla, boolean limpiar) throws Exception {
        
        this.principal = principal;
        this.component = component;
        this.plantilla = plantilla;
        this.simbolos = new HashMap<>();
        this.guias = new HashMap<>();
        this.bloqueos = new HashMap<>();
        System.out.println("Plantilla " + plantilla + " seleccionada.");

        crucigrama = CruciUtils.inicializar(plantilla);
        crucigramaPaint = new CasillaDto[crucigrama[0].length][crucigrama.length];
        for (int i = 0; i < crucigrama[0].length; i++) {
            for (int j = 0; j < crucigrama.length; j++) {
                CasillaDto casilla = new CasillaDto();
                casilla.setLetra(crucigrama[j][i]);
                crucigramaPaint[i][j] = casilla;
            }
        }
        if (limpiar) {
            CruciUtils.limpiar();
        }
    }

    public boolean procesar() {
        String[] inicios = Parametros.modelo1Inicios.split(",");
        String[] imagenes = Parametros.modelo1Imagenes.split(",");
        String[] rutas = Parametros.modelo1Rutas.split(",");
        List<Integer> idImagenes = new ArrayList<Integer>();
        switch (plantilla) {
            case Constantes.MODELO_1: {
                idImagenes.add(5);
                break;
            }
            case Constantes.MODELO_2: {
                inicios = Parametros.modelo2Inicios.split(",");
                imagenes = Parametros.modelo2Imagenes.split(",");
                rutas = Parametros.modelo2Rutas.split(",");
                idImagenes.add(5);
                break;
            }
            case Constantes.MODELO_3: {
                inicios = Parametros.modelo3Inicios.split(",");
                imagenes = Parametros.modelo3Imagenes.split(",");
                rutas = Parametros.modelo3Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_4: {
                inicios = Parametros.modelo4Inicios.split(",");
                imagenes = Parametros.modelo4Imagenes.split(",");
                rutas = Parametros.modelo4Rutas.split(",");
                idImagenes.add(1);
                break;
            }
            case Constantes.MODELO_5: {
                inicios = Parametros.modelo5Inicios.split(",");
                imagenes = Parametros.modelo5Imagenes.split(",");
                rutas = Parametros.modelo5Rutas.split(",");
                idImagenes.add(1);
                break;
            }
            case Constantes.MODELO_6: {
                inicios = Parametros.modelo6Inicios.split(",");
                imagenes = Parametros.modelo6Imagenes.split(",");
                rutas = Parametros.modelo6Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_7: {
                inicios = Parametros.modelo7Inicios.split(",");
                imagenes = Parametros.modelo7Imagenes.split(",");
                rutas = Parametros.modelo7Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(7);
                break;
            }
            case Constantes.MODELO_8: {
                inicios = Parametros.modelo8Inicios.split(",");
                imagenes = Parametros.modelo8Imagenes.split(",");
                rutas = Parametros.modelo8Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_9: {
                inicios = Parametros.modelo9Inicios.split(",");
                imagenes = Parametros.modelo9Imagenes.split(",");
                rutas = Parametros.modelo9Rutas.split(",");
                idImagenes.add(3);
                idImagenes.add(4);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_10: {
                inicios = Parametros.modelo10Inicios.split(",");
                imagenes = Parametros.modelo10Imagenes.split(",");
                rutas = Parametros.modelo10Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(6);
                idImagenes.add(7);
                break;
            }
            case Constantes.MODELO_11: {
                inicios = Parametros.modelo11Inicios.split(",");
                imagenes = Parametros.modelo11Imagenes.split(",");
                rutas = Parametros.modelo11Rutas.split(",");
                idImagenes.add(2);
                idImagenes.add(5);
                idImagenes.add(8);
                break;
            }
            case Constantes.MODELO_12: {
                inicios = Parametros.modelo12Inicios.split(",");
                imagenes = Parametros.modelo12Imagenes.split(",");
                rutas = Parametros.modelo12Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(5);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_13: {
                inicios = Parametros.modelo13Inicios.split(",");
                imagenes = Parametros.modelo13Imagenes.split(",");
                rutas = Parametros.modelo13Rutas.split(",");
                idImagenes.add(3);
                idImagenes.add(5);
                idImagenes.add(7);
                break;
            }
            case Constantes.MODELO_14: {
                inicios = Parametros.modelo14Inicios.split(",");
                imagenes = Parametros.modelo14Imagenes.split(",");
                rutas = Parametros.modelo14Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(3);
                idImagenes.add(7);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_15: {
                inicios = Parametros.modelo15Inicios.split(",");
                imagenes = Parametros.modelo15Imagenes.split(",");
                rutas = Parametros.modelo15Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(3);
                idImagenes.add(7);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_16: {
                inicios = Parametros.modelo16Inicios.split(",");
                imagenes = Parametros.modelo16Imagenes.split(",");
                rutas = Parametros.modelo16Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(3);
                idImagenes.add(5);
                idImagenes.add(7);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_17: {
                inicios = Parametros.modelo17Inicios.split(",");
                imagenes = Parametros.modelo17Imagenes.split(",");
                rutas = Parametros.modelo17Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(3);
                idImagenes.add(5);
                idImagenes.add(7);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_18: {
                inicios = Parametros.modelo18Inicios.split(",");
                imagenes = Parametros.modelo18Imagenes.split(",");
                rutas = Parametros.modelo18Rutas.split(",");
                idImagenes.add(1);
                idImagenes.add(3);
                idImagenes.add(4);
                idImagenes.add(6);
                idImagenes.add(7);
                idImagenes.add(9);
                break;
            }
            case Constantes.MODELO_19: {
                inicios = Parametros.modelo19Inicios.split(",");
                imagenes = Parametros.modelo19Imagenes.split(",");
                rutas = Parametros.modelo19Rutas.split(",");
                idImagenes.add(5);
                break;
            }
            case Constantes.MODELO_20: {
                inicios = Parametros.modelo20Inicios.split(",");
                imagenes = Parametros.modelo20Imagenes.split(",");
                rutas = Parametros.modelo20Rutas.split(",");
                idImagenes.add(5);
                break;
            }
        }
        boolean ok = true;
        for(int i = 0; i < inicios.length && ok; i++){
            int indice = idImagenes.get(i);
            String palabra = principal.getPalabra(indice);
            Imagen imagen = CruciUtils.obtenerImagen(i, inicios, imagenes);
            System.out.println(imagen.toString());
            ok = colocarPalabraGenerica(palabra, imagen, rutas[i]);
        }

        if(ok){
            // RELLENAR DEMAS CASILLAS
            for (int i = 0; i < 20; i++) {
                generarAleatorioHorizontalBack();
                generarAleatorioVerticalBack();                
                generarAleatorioVertical();
                generarAleatorioHorizontal();
            }
            generarRellenoHorizontal();
            generarRellenoVertical();
            return true;
        }
        return false;
    }

    public boolean existe() throws Exception {
        Connection conn = DatabaseAccess.abrir();
        boolean response = DatabaseAccess.existe(conn);
        DatabaseAccess.cerrar(conn);
        return response;
    }

    private boolean colocarPalabraGenerica(String txtPalabra, Imagen imagen, String ruta) {
        // TODO: PROCESAR LA RUTA
        int direccion = CruciUtils.direccionInicial(ruta);
        Posicion posicion = new Posicion();
        posicion.setX(imagen.getX());
        posicion.setY(imagen.getY());
        posicion.setDireccion(direccion);

        List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, ruta);
        while(txtPalabra.contains("  ")){
            txtPalabra = txtPalabra.replace("  ", " ");
        }
        txtPalabra = txtPalabra.toUpperCase();
        Palabra palabra = new Palabra();
        palabra.setId(0);
        palabra.setDescripcion("");
        palabra.setPalabra(txtPalabra);
        palabra.setCantidad(txtPalabra.length());

        //COLOCAR PRIMER LETRA
        char letra = palabra.getPalabra().charAt(0);
        crucigrama[posicion.getY()][posicion.getX()] = String.valueOf(letra);
        CasillaDto casilla = crucigramaPaint[posicion.getX()][posicion.getY()];
        casilla.setLetra(String.valueOf(letra));

        //BLOQUEO INICIAL
        Bloqueos resp = CruciUtils.bloqueoInicial(posicion);
        bloqueos.put(new Posicion(posicion.getX(), posicion.getY()), resp);

        //GUARDAR SIMBOLO INICIAL
        Simbolo simboloInicial = CruciUtils.simbolo(posicion, posicion, false, true, casilla);
        Posicion auxPos1 = new Posicion(posicion.getX(), posicion.getY());
        List<Simbolo> auxSimbolos1 = simbolos.get(auxPos1);
        auxSimbolos1 = (auxSimbolos1 == null) ? new ArrayList<>() : auxSimbolos1;
        auxSimbolos1.add(simboloInicial);
        simbolos.put(auxPos1, auxSimbolos1);

        int contador = 0;
        for (int i = 1; i < palabra.getPalabra().length(); i++) {
            contador++;
            letra = palabra.getPalabra().charAt(i);
            Posicion actual = rutaMapa.get(contador);
            if (String.valueOf(letra).equals(" ")) {
                contador--;
                //OBTENER LA POSICION DONDE SE DEBE COLOCAR LA LETRA EN CURSO
                Simbolo simboloEspacio = CruciUtils.simboloEspacio(actual);
                Posicion auxPos2 = new Posicion(actual.getX(), actual.getY());
                List<Simbolo> auxSimbolos2 = simbolos.get(auxPos2);
                auxSimbolos2 = (auxSimbolos2 == null) ? new ArrayList<>() : auxSimbolos2;
                auxSimbolos2.add(simboloEspacio);
                simbolos.put(auxPos2, auxSimbolos2);
            } else {
                //OBTENER LA POSICION DONDE SE DEBE COLOCAR LA LETRA EN CURSO
                crucigrama[actual.getY()][actual.getX()] = String.valueOf(letra);
                casilla = crucigramaPaint[actual.getX()][actual.getY()];
                casilla.setLetra(String.valueOf(letra));

                //BLOQUEOS INTERMEDIOS
                resp = CruciUtils.bloqueoIntermedio(posicion, actual);
                bloqueos.put(new Posicion(actual.getX(), actual.getY()), resp);

                // SI LA PALABRA CAMBIA DE DIRECCION GUARDAR EL SIMBOLO EN LA CASILLA
                if (actual.getDireccion() != posicion.getDireccion()) {
                    bloqueos.put(new Posicion(posicion.getX(), posicion.getY()), resp);

                    Simbolo simboloIntermedio = CruciUtils.simbolo(posicion, actual, false, false, casilla);
                    Posicion auxPos2 = new Posicion(posicion.getX(), posicion.getY());
                    List<Simbolo> auxSimbolos2 = simbolos.get(auxPos2);
                    auxSimbolos2 = (auxSimbolos2 == null) ? new ArrayList<>() : auxSimbolos2;
                    auxSimbolos2.add(simboloIntermedio);
                    simbolos.put(auxPos2, auxSimbolos2);
                }
                posicion = actual;
            }
        }

        //BLOQUEO FINAL
        resp = CruciUtils.bloqueoInicial(posicion);
        bloqueos.put(new Posicion(posicion.getX(), posicion.getY()), resp);
        casilla = crucigramaPaint[posicion.getX()][posicion.getY()];

        //GUARDAR EL SIMBOLO DE FINALIZACION DE LA PALABRA EN LA ULTIMA CASILLA
        Simbolo simboloFinal = CruciUtils.simbolo(posicion, posicion, true, false, casilla);
        Posicion auxPos3 = new Posicion(posicion.getX(), posicion.getY());
        List<Simbolo> auxSimbolos3 = simbolos.get(auxPos3);
        auxSimbolos3 = (auxSimbolos3 == null) ? new ArrayList<>() : auxSimbolos3;
        auxSimbolos3.add(simboloFinal);
        simbolos.put(auxPos3, auxSimbolos3);
        return true;
    }

    private void generarHorizontal(int randomX, int randomY) {
        try {
            int contador = 0;
            Condicion condicion = null;
            boolean reiniciar = true;
            for (int j = randomY; j < MAX_Y; j++) {
                for (int i = randomX; i < MAX_X; i++) {
                    String casilla = crucigrama[j][i];
                    
                    if(randomX == 0 && randomY == 16) {
                        System.out.println("joker:" + casilla);
                    }
                    
                   char flag = casilla.charAt(0);
                    switch (flag) {
                        case 46: { 
                           if (reiniciar) {
                                reiniciar = false;
                                contador = 0;
                                condicion = new Condicion();
                                int direccion = Constantes.DIRECCION_DERECHA;
                                condicion.setInicio(new Posicion(i, j, direccion));
                                condicion.setCondiciones(new HashMap<>());
                            }
                            //VACIO
                            contador++;
                            break;
                        }
                        case 64:
                        case 35: {
                            //IMAGEN o PUBLICIDAD
                            if (reiniciar) {
                                contador = 0;
                            }
                            if (contador > 0) {
                                reiniciar = true;
                                //buscar palabra y colocar
                                condicion.setLongitud(contador);
                                System.out.println("Buscar palabra de " + contador + " caracteres.");
                                Palabra palabra = CruciUtils.buscar(condicion);
                                if (palabra != null) {
                                    System.out.println("Palabra encontrada: " + palabra.getPalabra());
                                    colocarPalabraHorizontal(condicion.getInicio(), palabra);

                                    int delta = contador - palabra.getPalabra().length();
                                    if (delta > 0) {
                                        //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                        i -= delta;
                                    }
                                }
                                contador = 0;
                            }
                            break;
                        }
                        default: {
                            //OCUPADO
                            Posicion p = new Posicion(i, j);
                            Bloqueos bloqueo = bloqueos.get(p);
                            boolean permisible = !bloqueo.isBloqueoIzquierda();
                            if (permisible) {
                                if (reiniciar) {
                                    reiniciar = false;
                                    contador = 0;
                                    condicion = new Condicion();
                                    int direccion = Constantes.DIRECCION_DERECHA;
                                    condicion.setInicio(new Posicion(i, j, direccion));
                                    condicion.setCondiciones(new HashMap<>());
                                }
                                contador++;
                                condicion.getCondiciones().put(contador, casilla);
                                permisible = !bloqueo.isBloqueoDerecha();
                                if (!permisible) {
                                    if (contador > 1) {
                                        reiniciar = true;
                                        //buscar palabra y colocar
                                        condicion.setLongitud(contador);
                                        System.out.println("Buscar palabra de " + contador + " caracteres.");
                                        Palabra palabra = CruciUtils.buscar(condicion);
                                        if (palabra != null) {
                                            System.out.println("Palabra encontrada: " + palabra.getPalabra());
                                            colocarPalabraHorizontal(condicion.getInicio(), palabra);

                                            int delta = contador - palabra.getPalabra().length();
                                            if (delta > 0) {
                                                //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                                i -= delta;
                                            }
                                        }
                                        contador = 0;
                                    }
                                }
                            } else {
                                if (reiniciar) {
                                    contador = 0;
                                }
                                if (contador > 0) {
                                    reiniciar = true;
                                    //buscar palabra y colocar
                                    condicion.setLongitud(contador);
                                    System.out.println("Buscar palabra de " + contador + " caracteres.");
                                    Palabra palabra = CruciUtils.buscar(condicion);
                                    if (palabra != null) {
                                        System.out.println("Palabra encontrada: " + palabra.getPalabra());
                                        colocarPalabraHorizontal(condicion.getInicio(), palabra);

                                        int delta = contador - palabra.getPalabra().length();
                                        if (delta > 0) {
                                            //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                            i -= delta;
                                        }
                                    }
                                    contador = 0;
                                }
                            }
                            break;
                        }
                    }
                    if ((i + 1) >= MAX_X && contador > 0) {
                        //Si se llega al final de la linea
                        //buscar palabra y colocar
                        reiniciar = true;
                        condicion.setLongitud(contador);
                        System.out.println("Buscar palabra de " + contador + " caracteres.");
                        Palabra palabra = CruciUtils.buscar(condicion);
                        if (palabra != null) {
                            System.out.println("Palabra encontrada: " + palabra.getPalabra());
                            colocarPalabraHorizontal(condicion.getInicio(), palabra);

                            int delta = contador - palabra.getPalabra().length();
                            if (delta > 0) {
                                //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                i -= delta;
                                contador = 0;
                            }
                        }
                    }
                }
                reiniciar = true;
                if (randomY + randomX > 0) {
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generarVertical(int randomX, int randomY) {
        try {
            int contador = 0;
            Condicion condicion = null;
            boolean reiniciar = true;
            for (int i = randomX; i < MAX_X; i++) {/**/
                for (int j = randomY; j < MAX_Y; j++) {
                    String casilla = crucigrama[j][i];
                    switch (casilla.charAt(0)) {
                        case 46: {
                            if (reiniciar) {
                                reiniciar = false;
                                contador = 0;
                                condicion = new Condicion();
                                int direccion = Constantes.DIRECCION_ABAJO;/**/

                                condicion.setInicio(new Posicion(i, j, direccion));
                                condicion.setCondiciones(new HashMap<Integer, String>());
                            }
                            //VACIO
                            contador++;
                            break;
                        }
                        case 64:
                        case 35: {
                            //IMAGEN o PUBLICIDAD
                            if (reiniciar) {
                                contador = 0;
                            }
                            if (contador > 0) {
                                reiniciar = true;
                                //buscar palabra y colocar
                                condicion.setLongitud(contador);
                                System.out.println("Buscar palabra de " + contador + " caracteres.");
                                Palabra palabra = CruciUtils.buscar(condicion);
                                if (palabra != null) {
                                    System.out.println("Palabra encontrada: " + palabra.getPalabra());
                                    colocarPalabraVertical(condicion.getInicio(), palabra);/**/

                                    int delta = contador - palabra.getCantidad();
                                    if (delta > 0) {
                                        //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                        j -= delta;
                                    }
                                }
                            }
                            break;
                        }
                        default: {
                            //OCUPADO
                            Posicion p = new Posicion(i, j);
                            Bloqueos bloqueo = bloqueos.get(p);
                            boolean permisible = !bloqueo.isBloqueoArriba();/**/

                            if (permisible) {
                                if (reiniciar) {
                                    reiniciar = false;
                                    contador = 0;
                                    condicion = new Condicion();
                                    int direccion = Constantes.DIRECCION_ABAJO;/**/

                                    condicion.setInicio(new Posicion(i, j, direccion));
                                    condicion.setCondiciones(new HashMap<Integer, String>());
                                }
                                contador++;
                                condicion.getCondiciones().put(contador, casilla);
                                permisible = !bloqueo.isBloqueoAbajo();/**/

                                if (!permisible) {
                                    reiniciar = true;
                                    //buscar palabra y colocar
                                    condicion.setLongitud(contador);
                                    System.out.println("Buscar palabra de " + contador + " caracteres.");
                                    Palabra palabra = CruciUtils.buscar(condicion);
                                    if (palabra != null) {
                                        System.out.println("Palabra encontrada: " + palabra.getPalabra());
                                        colocarPalabraVertical(condicion.getInicio(), palabra);/**/

                                        int delta = contador - palabra.getPalabra().length();
                                        if (delta > 0) {
                                            //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                            j -= delta;
                                        }
                                    }
                                }
                            } else {
                                if (reiniciar) {
                                    contador = 0;
                                }
                                if (contador > 0) {
                                    reiniciar = true;
                                    //buscar palabra y colocar
                                    condicion.setLongitud(contador);
                                    System.out.println("Buscar palabra de " + contador + " caracteres.");
                                    Palabra palabra = CruciUtils.buscar(condicion);
                                    if (palabra != null) {
                                        System.out.println("Palabra encontrada: " + palabra.getPalabra());
                                        colocarPalabraVertical(condicion.getInicio(), palabra);/**/

                                        int delta = contador - palabra.getPalabra().length();
                                        if (delta > 0) {
                                            //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                            j -= delta;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                    if ((j + 1) >= MAX_Y && contador > 0) {
                        //Si se llega al final de la linea
                        //buscar palabra y colocar
                        reiniciar = true;
                        condicion.setLongitud(contador);
                        System.out.println("Buscar palabra de " + contador + " caracteres.");
                        Palabra palabra = CruciUtils.buscar(condicion);
                        if (palabra != null) {
                            System.out.println("Palabra encontrada: " + palabra.getPalabra());
                            colocarPalabraVertical(condicion.getInicio(), palabra);/**/

                            int delta = contador - palabra.getPalabra().length();
                            if (delta > 0) {
                                //Cuando no se encuentra palabra de la cantidad buscada, recorrer el indice
                                j -= delta;
                                contador = 0;
                            }
                        }
                    }
                }
                reiniciar = true;
                if (randomY + randomX > 0) {
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void colocarPalabraHorizontal(Posicion inicio, Palabra palabra) {
        //COLOCAR PRIMER LETRA
        char letra = palabra.getPalabra().charAt(0);
        crucigrama[inicio.getY()][inicio.getX()] = String.valueOf(letra);
        CasillaDto casilla = crucigramaPaint[inicio.getX()][inicio.getY()];
        casilla.setLetra(String.valueOf(letra));

        //BLOQUEO INICIAL
        Bloqueos resp = CruciUtils.bloqueoInicial(inicio);
        Posicion p = new Posicion(inicio.getX(), inicio.getY());
        Bloqueos last = bloqueos.get(p);
        if (last == null) {
            last = new Bloqueos(false, false, false, false);
        }
        last.setBloqueoAbajo(last.isBloqueoAbajo() || resp.isBloqueoAbajo());
        last.setBloqueoArriba(last.isBloqueoArriba() || resp.isBloqueoArriba());
        last.setBloqueoDerecha(last.isBloqueoDerecha() || resp.isBloqueoDerecha());
        last.setBloqueoIzquierda(last.isBloqueoIzquierda() || resp.isBloqueoIzquierda());
        bloqueos.put(p, last);

        //GUARDAR SIMBOLO INICIAL
        Leyenda guia = CruciUtils.guia(inicio, palabra, casilla);
        Posicion auxPo1 = new Posicion(inicio.getX(), inicio.getY());
        List<Leyenda> auxLeyenda1 = guias.get(auxPo1);
        auxLeyenda1 = (auxLeyenda1 == null) ? new ArrayList<>() : auxLeyenda1;
        auxLeyenda1.add(guia);
        guias.put(auxPo1, auxLeyenda1);

        if (palabra.getCantidad() > 1) {
            for (int i = 1; i < palabra.getPalabra().length(); i++) {
                try {
                    inicio.setX(inicio.getX() + 1);
                    letra = palabra.getPalabra().charAt(i);
                    if (String.valueOf(letra).equals(" ")) {
                        Simbolo simboloEspacio = CruciUtils.simboloEspacio(inicio);
                        Posicion auxPos2 = new Posicion(inicio.getX(), inicio.getY());
                        List<Simbolo> auxSimbolos2 = simbolos.get(auxPos2);
                        auxSimbolos2 = (auxSimbolos2 == null) ? new ArrayList<>() : auxSimbolos2;
                        auxSimbolos2.add(simboloEspacio);
                        simbolos.put(auxPos2, auxSimbolos2);
                        inicio.setX(inicio.getX() - 1);
                    } else {
                        crucigrama[inicio.getY()][inicio.getX()] = String.valueOf(letra);
                        casilla = crucigramaPaint[inicio.getX()][inicio.getY()];
                        casilla.setLetra(String.valueOf(letra));

                        //BLOQUEOS INTERMEDIOS
                        resp = CruciUtils.bloqueoIntermedio(inicio, inicio);
                        Posicion posB = new Posicion(inicio.getX(), inicio.getY());
                        Bloqueos lastB = bloqueos.get(posB);
                        if (lastB == null) {
                            lastB = new Bloqueos(false, false, false, false);
                        }
                        lastB.setBloqueoAbajo(lastB.isBloqueoAbajo() || resp.isBloqueoAbajo());
                        lastB.setBloqueoArriba(lastB.isBloqueoArriba() || resp.isBloqueoArriba());
                        lastB.setBloqueoDerecha(lastB.isBloqueoDerecha() || resp.isBloqueoDerecha());
                        lastB.setBloqueoIzquierda(lastB.isBloqueoIzquierda() || resp.isBloqueoIzquierda());
                        bloqueos.put(posB, lastB);
                    }
                } catch (Exception e) {
                }
            }
        }

        //BLOQUEO FINAL
        resp = CruciUtils.bloqueoInicial(inicio);
        Posicion pos = new Posicion(inicio.getX(), inicio.getY());
        Bloqueos lastB = bloqueos.get(pos);
        if (lastB == null) {
            lastB = new Bloqueos(false, false, false, false);
        }
        lastB.setBloqueoAbajo(lastB.isBloqueoAbajo() || resp.isBloqueoAbajo());
        lastB.setBloqueoArriba(lastB.isBloqueoArriba() || resp.isBloqueoArriba());
        lastB.setBloqueoDerecha(lastB.isBloqueoDerecha() || resp.isBloqueoDerecha());
        lastB.setBloqueoIzquierda(lastB.isBloqueoIzquierda() || resp.isBloqueoIzquierda());
        bloqueos.put(pos, lastB);
        casilla = crucigramaPaint[inicio.getX()][inicio.getY()];

        //GUARDAR EL SIMBOLO DE FINALIZACION DE LA PALABRA EN LA ULTIMA CASILLA
        if(inicio.getX() + 1 < MAX_X){
            Simbolo simboloFinal = CruciUtils.simbolo(inicio, inicio, true, false, casilla);
            Posicion auxPos3 = new Posicion(inicio.getX(), inicio.getY());
            List<Simbolo> auxSimbolos3 = simbolos.get(auxPos3);
            auxSimbolos3 = (auxSimbolos3 == null) ? new ArrayList<>() : auxSimbolos3;
            auxSimbolos3.add(simboloFinal);
            simbolos.put(auxPos3, auxSimbolos3);
        }
    }

    private void colocarPalabraVertical(Posicion inicio, Palabra palabra) {
        //COLOCAR PRIMER LETRA
        char letra = palabra.getPalabra().charAt(0);
        crucigrama[inicio.getY()][inicio.getX()] = String.valueOf(letra);
        CasillaDto casilla = crucigramaPaint[inicio.getX()][inicio.getY()];
        casilla.setLetra(String.valueOf(letra));

        //BLOQUEO INICIAL
        Bloqueos resp = CruciUtils.bloqueoInicial(inicio);
        Posicion p = new Posicion(inicio.getX(), inicio.getY());
        Bloqueos last = bloqueos.get(p);
        if (last == null) {
            last = new Bloqueos(false, false, false, false);
        }
        last.setBloqueoAbajo(last.isBloqueoAbajo() || resp.isBloqueoAbajo());
        last.setBloqueoArriba(last.isBloqueoArriba() || resp.isBloqueoArriba());
        last.setBloqueoDerecha(last.isBloqueoDerecha() || resp.isBloqueoDerecha());
        last.setBloqueoIzquierda(last.isBloqueoIzquierda() || resp.isBloqueoIzquierda());
        bloqueos.put(p, last);

        //GUARDAR SIMBOLO INICIAL
        Leyenda guia = CruciUtils.guia(inicio, palabra, casilla);
        Posicion auxPo1 = new Posicion(inicio.getX(), inicio.getY());
        List<Leyenda> auxLeyenda1 = guias.get(auxPo1);
        auxLeyenda1 = (auxLeyenda1 == null) ? new ArrayList<>() : auxLeyenda1;
        auxLeyenda1.add(guia);
        guias.put(auxPo1, auxLeyenda1);

        if (palabra.getCantidad() > 1) {
            for (int pos = 1; pos < palabra.getPalabra().length(); pos++) {
                try {
                    inicio.setY(inicio.getY() + 1);
                    letra = palabra.getPalabra().charAt(pos);
                    if (String.valueOf(letra).equals(" ")) {
                        Simbolo simboloEspacio = CruciUtils.simboloEspacio(inicio);
                        Posicion auxPos2 = new Posicion(inicio.getX(), inicio.getY());
                        List<Simbolo> auxSimbolos2 = simbolos.get(auxPos2);
                        auxSimbolos2 = (auxSimbolos2 == null) ? new ArrayList<>() : auxSimbolos2;
                        auxSimbolos2.add(simboloEspacio);
                        simbolos.put(auxPos2, auxSimbolos2);
                        inicio.setX(inicio.getX() - 1);
                    } else {
                        crucigrama[inicio.getY()][inicio.getX()] = String.valueOf(letra);
                        casilla = crucigramaPaint[inicio.getX()][inicio.getY()];
                        casilla.setLetra(String.valueOf(letra));

                        //BLOQUEOS INTERMEDIOS
                        resp = CruciUtils.bloqueoIntermedio(inicio, inicio);
                        Posicion posB = new Posicion(inicio.getX(), inicio.getY());
                        Bloqueos lastB = bloqueos.get(posB);
                        if (lastB == null) {
                            lastB = new Bloqueos(false, false, false, false);
                        }
                        lastB.setBloqueoAbajo(lastB.isBloqueoAbajo() || resp.isBloqueoAbajo());
                        lastB.setBloqueoArriba(lastB.isBloqueoArriba() || resp.isBloqueoArriba());
                        lastB.setBloqueoDerecha(lastB.isBloqueoDerecha() || resp.isBloqueoDerecha());
                        lastB.setBloqueoIzquierda(lastB.isBloqueoIzquierda() || resp.isBloqueoIzquierda());
                        bloqueos.put(posB, lastB);
                    }
                } catch (Exception e) {
                }
            }
        }

        //BLOQUEO FINAL
        resp = CruciUtils.bloqueoInicial(inicio);
        Posicion pos = new Posicion(inicio.getX(), inicio.getY());
        Bloqueos lastB = bloqueos.get(pos);
        if (lastB == null) {
            lastB = new Bloqueos(false, false, false, false);
        }
        lastB.setBloqueoAbajo(lastB.isBloqueoAbajo() || resp.isBloqueoAbajo());
        lastB.setBloqueoArriba(lastB.isBloqueoArriba() || resp.isBloqueoArriba());
        lastB.setBloqueoDerecha(lastB.isBloqueoDerecha() || resp.isBloqueoDerecha());
        lastB.setBloqueoIzquierda(lastB.isBloqueoIzquierda() || resp.isBloqueoIzquierda());
        bloqueos.put(pos, lastB);
        

        //GUARDAR EL SIMBOLO DE FINALIZACION DE LA PALABRA EN LA ULTIMA CASILLA
        if(inicio.getY() + 1 < MAX_Y){
            casilla = crucigramaPaint[inicio.getX()][inicio.getY()];
            Simbolo simboloFinal = CruciUtils.simbolo(inicio, inicio, true, false, casilla);
            Posicion auxPos3 = new Posicion(inicio.getX(), inicio.getY());
            List<Simbolo> auxSimbolos3 = simbolos.get(auxPos3);
            auxSimbolos3 = (auxSimbolos3 == null) ? new ArrayList<>() : auxSimbolos3;
            auxSimbolos3.add(simboloFinal);
            simbolos.put(auxPos3, auxSimbolos3);
        } 
    }

    public String[][] getCrucigrama() {
        return crucigrama;
    }

    public Map<Posicion, List<Simbolo>> getSimbolos() {
        return simbolos;
    }

    public Map<Posicion, List<Leyenda>> getGuias() {
        return guias;
    }

    private void generarAleatorioHorizontal() {
        System.out.println("***********************");
        System.out.println("Generando horizontales...");
        System.out.println("***********************");

        for (int i = 0; i < crucigrama.length; i++) {
            int random = (int) (Math.random() * 13);
            generarHorizontal(random, i);
        }
    }

    private void generarAleatorioVertical() {
        System.out.println("***********************");
        System.out.println("Generando verticales...");
        System.out.println("***********************");

        String[] fila = crucigrama[0];
        for (int i = 0; i < fila.length; i++) {
            int random = (int) (Math.random() * 15);
            generarVertical(i, random);
        }
    }

    private void generarAleatorioHorizontalBack() {
        System.out.println("***********************");
        System.out.println("Generando horizontales...");
        System.out.println("***********************");

        for (int i = crucigrama.length - 1; i >= 0; i--) {
            int random = (int) (Math.random() * 10);
            generarHorizontal(random, i);
        }
    }

    private void generarAleatorioVerticalBack() {
        System.out.println("***********************");
        System.out.println("Generando verticales back...");
        System.out.println("***********************");

        String[] fila = crucigrama[0];
        for (int i = fila.length - 1; i >= 0; i--) {
            int random = (int) (Math.random() * 10);
            generarVertical(i, random);
        }
    }

    private void generarRellenoHorizontal() {
        System.out.println("***********************");
        System.out.println("Generando horizontales...");
        System.out.println("***********************");
        generarHorizontal(0, 0);
    }

    private void generarRellenoVertical() {
        System.out.println("***********************");
        System.out.println("Generando verticales...");
        System.out.println("***********************");
        generarVertical(0, 0);
    }

    public CasillaDto[][] getCrucigramaPaint() {
        return crucigramaPaint;
    }
}
