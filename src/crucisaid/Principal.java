package crucisaid;

import crucisaid.logic.CruciUtils;
import crucisaid.logic.Crucigrama;
import crucisaid.model.Imagen;
import crucisaid.model.Leyenda;
import crucisaid.model.Posicion;
import crucisaid.model.Simbolo;
import crucisaid.utils.Constantes;
import crucisaid.utils.Parametros;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.metal.MetalToggleButtonUI;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class Principal extends javax.swing.JFrame {

    public static final int ANCHO = 1200;
    public static final int ALTO = 780;
    private static final String PATTERN = "[^\\\\A-Za-z\\s\\d]";

    private static Principal principal;
    private static FramePalabras framePalabras;

    private Map<Posicion, List<Simbolo>> simbolos;
    private Map<Posicion, List<Leyenda>> guias;
    private String[][] crucigrama;
    private int plantilla = 1;
    private List<Integer> limitesTexto;

    private boolean nuevo = true;
    private boolean limpiar = true;
    private boolean mostrarLetras = true;
    private boolean mostrarSimbolos = true;
    private boolean mostrarGuia = true;

    private JFrame waitingDialog;

    public Principal() {
        initComponents();

        JLabel waitingIcon = new JLabel();
        waitingIcon.setIcon(new ImageIcon(getClass().getResource("/loading.gif")));
        this.waitingDialog = new JFrame("");
        this.waitingDialog.setSize(70, 70);
        this.waitingDialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.waitingDialog.getContentPane().add(waitingIcon);
        this.waitingDialog.getContentPane().setBackground(Color.white);
        this.waitingDialog.pack();
        this.waitingDialog.setLocationRelativeTo(null);
        this.limitesTexto = new ArrayList<Integer>();
        for (int i = 0; i < 9; i++) {
            limitesTexto.add(0);
        }
        this.actualizarPalabras();
        this.initBotones();
        this.actualizarBotones();
    }

    private void initBotones() {
        MetalToggleButtonUI m = new MetalToggleButtonUI() {
            @Override
            protected Color getSelectColor() {
                return Color.RED;
            }
        };
        btnModelo01.setUI(m);
        btnModelo02.setUI(m);
        btnModelo03.setUI(m);
        btnModelo04.setUI(m);
        btnModelo05.setUI(m);
        btnModelo06.setUI(m);
        btnModelo07.setUI(m);
        btnModelo08.setUI(m);
        btnModelo09.setUI(m);
        btnModelo10.setUI(m);
        btnModelo11.setUI(m);
        btnModelo12.setUI(m);
        btnModelo13.setUI(m);
        btnModelo14.setUI(m);
        btnModelo15.setUI(m);
        btnModelo16.setUI(m);
        btnModelo17.setUI(m);
        btnModelo18.setUI(m);
        btnModelo19.setUI(m);
        btnModelo20.setUI(m);
    }

    public void actualizarBotones() {
        btnModelo01.setSelected(false);
        btnModelo02.setSelected(false);
        btnModelo03.setSelected(false);
        btnModelo04.setSelected(false);
        btnModelo05.setSelected(false);
        btnModelo06.setSelected(false);
        btnModelo07.setSelected(false);
        btnModelo08.setSelected(false);
        btnModelo09.setSelected(false);
        btnModelo10.setSelected(false);
        btnModelo11.setSelected(false);
        btnModelo12.setSelected(false);
        btnModelo13.setSelected(false);
        btnModelo14.setSelected(false);
        btnModelo15.setSelected(false);
        btnModelo16.setSelected(false);
        btnModelo17.setSelected(false);
        btnModelo18.setSelected(false);
        btnModelo19.setSelected(false);
        btnModelo20.setSelected(false);
        switch (plantilla) {
            case 1: {
                btnModelo01.setSelected(true);
                break;
            }
            case 2: {
                btnModelo02.setSelected(true);
                break;
            }
            case 3: {
                btnModelo03.setSelected(true);
                break;
            }
            case 4: {
                btnModelo04.setSelected(true);
                break;
            }
            case 5: {
                btnModelo05.setSelected(true);
                break;
            }
            case 6: {
                btnModelo06.setSelected(true);
                break;
            }
            case 7: {
                btnModelo07.setSelected(true);
                break;
            }
            case 8: {
                btnModelo08.setSelected(true);
                break;
            }
            case 9: {
                btnModelo09.setSelected(true);
                break;
            }
            case 10: {
                btnModelo10.setSelected(true);
                break;
            }
            case 11: {
                btnModelo11.setSelected(true);
                break;
            }
            case 12: {
                btnModelo12.setSelected(true);
                break;
            }
            case 13: {
                btnModelo13.setSelected(true);
                break;
            }
            case 14: {
                btnModelo14.setSelected(true);
                break;
            }
            case 15: {
                btnModelo15.setSelected(true);
                break;
            }
            case 16: {
                btnModelo16.setSelected(true);
                break;
            }
            case 17: {
                btnModelo17.setSelected(true);
                break;
            }
            case 18: {
                btnModelo18.setSelected(true);
                break;
            }
            case 19: {
                btnModelo19.setSelected(true);
                break;
            }
            case 20: {
                btnModelo20.setSelected(true);
                break;
            }
        }
    }

    public void actualizarPalabras() {
        txtPalabra1.setText("");
        txtPalabra2.setText("");
        txtPalabra3.setText("");
        txtPalabra4.setText("");
        txtPalabra5.setText("");
        txtPalabra6.setText("");
        txtPalabra7.setText("");
        txtPalabra8.setText("");
        txtPalabra9.setText("");
        switch (plantilla) {
            case 1: {
                String[] inicios = Parametros.modelo1Inicios.split(",");
                String[] imagenes = Parametros.modelo1Imagenes.split(",");
                String[] rutas = Parametros.modelo1Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                lblPalabra1.setVisible(false);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);
                txtPalabra1.setVisible(false);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);
                jblLong1.setVisible(false);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);
                cleanData();
                break;
            }
            case 2: {
                String[] inicios = Parametros.modelo2Inicios.split(",");
                String[] imagenes = Parametros.modelo2Imagenes.split(",");
                String[] rutas = Parametros.modelo2Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                lblPalabra1.setVisible(false);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);
                txtPalabra1.setVisible(false);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(false);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
            case 3: {
                String[] inicios = Parametros.modelo3Inicios.split(",");
                String[] imagenes = Parametros.modelo3Imagenes.split(",");
                String[] rutas = Parametros.modelo3Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);
                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 4: {
                String[] inicios = Parametros.modelo4Inicios.split(",");
                String[] imagenes = Parametros.modelo4Imagenes.split(",");
                String[] rutas = Parametros.modelo4Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);
                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
            case 5: {
                String[] inicios = Parametros.modelo5Inicios.split(",");
                String[] imagenes = Parametros.modelo5Imagenes.split(",");
                String[] rutas = Parametros.modelo5Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);
                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
            case 6: {
                String[] inicios = Parametros.modelo6Inicios.split(",");
                String[] imagenes = Parametros.modelo6Imagenes.split(",");
                String[] rutas = Parametros.modelo6Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);
                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);
                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);
                cleanData();
                break;
            }
            case 7: {
                String[] inicios = Parametros.modelo7Inicios.split(",");
                String[] imagenes = Parametros.modelo7Imagenes.split(",");
                String[] rutas = Parametros.modelo7Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);
                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
            case 8: {
                String[] inicios = Parametros.modelo8Inicios.split(",");
                String[] imagenes = Parametros.modelo8Imagenes.split(",");
                String[] rutas = Parametros.modelo8Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);
                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 9: {
                String[] inicios = Parametros.modelo9Inicios.split(",");
                String[] imagenes = Parametros.modelo9Imagenes.split(",");
                String[] rutas = Parametros.modelo9Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(2, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(3, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(false);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(true);
                lblPalabra4.setVisible(true);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);

                txtPalabra1.setVisible(false);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(true);
                txtPalabra4.setVisible(true);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(false);
                jblLong2.setVisible(false);
                jblLong3.setVisible(true);
                jblLong4.setVisible(true);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 10: {
                String[] inicios = Parametros.modelo10Inicios.split(",");
                String[] imagenes = Parametros.modelo10Imagenes.split(",");
                String[] rutas = Parametros.modelo10Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(5, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(true);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);

                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(true);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(true);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
            case 11: {
                String[] inicios = Parametros.modelo11Inicios.split(",");
                String[] imagenes = Parametros.modelo11Imagenes.split(",");
                String[] rutas = Parametros.modelo11Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(1, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(7, maxLongitud);

                lblPalabra1.setVisible(false);
                lblPalabra2.setVisible(true);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(true);
                lblPalabra9.setVisible(false);

                txtPalabra1.setVisible(false);
                txtPalabra2.setVisible(true);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(true);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(false);
                jblLong2.setVisible(true);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(true);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
            case 12: {
                String[] inicios = Parametros.modelo12Inicios.split(",");
                String[] imagenes = Parametros.modelo12Imagenes.split(",");
                String[] rutas = Parametros.modelo12Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);

                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 13: {
                String[] inicios = Parametros.modelo13Inicios.split(",");
                String[] imagenes = Parametros.modelo13Imagenes.split(",");
                String[] rutas = Parametros.modelo13Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(2, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                lblPalabra1.setVisible(false);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(true);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);

                txtPalabra1.setVisible(false);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(true);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(false);
                jblLong2.setVisible(false);
                jblLong3.setVisible(true);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
            case 14: {
                String[] inicios = Parametros.modelo14Inicios.split(",");
                String[] imagenes = Parametros.modelo14Imagenes.split(",");
                String[] rutas = Parametros.modelo14Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(2, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                imagen = CruciUtils.obtenerImagen(3, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[3]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[3]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(true);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);

                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(true);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(true);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 15: {
                String[] inicios = Parametros.modelo15Inicios.split(",");
                String[] imagenes = Parametros.modelo15Imagenes.split(",");
                String[] rutas = Parametros.modelo15Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(2, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                imagen = CruciUtils.obtenerImagen(3, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[3]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[3]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(true);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);

                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(true);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(true);
                jblLong4.setVisible(false);
                jblLong5.setVisible(false);
                jblLong6.setVisible(false);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 16: {
                String[] inicios = Parametros.modelo16Inicios.split(",");
                String[] imagenes = Parametros.modelo16Imagenes.split(",");
                String[] rutas = Parametros.modelo16Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(2, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                imagen = CruciUtils.obtenerImagen(3, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[3]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[3]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                imagen = CruciUtils.obtenerImagen(4, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[4]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[4]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(true);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);

                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(true);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(true);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;

            }
            case 17: {
                String[] inicios = Parametros.modelo17Inicios.split(",");
                String[] imagenes = Parametros.modelo17Imagenes.split(",");
                String[] rutas = Parametros.modelo17Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(2, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                imagen = CruciUtils.obtenerImagen(3, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[3]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[3]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                imagen = CruciUtils.obtenerImagen(4, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[4]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[4]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(true);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);

                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(true);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(true);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 18: {
                String[] inicios = Parametros.modelo18Inicios.split(",");
                String[] imagenes = Parametros.modelo18Imagenes.split(",");
                String[] rutas = Parametros.modelo18Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(0, maxLongitud);

                imagen = CruciUtils.obtenerImagen(1, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[1]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[1]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(2, maxLongitud);

                imagen = CruciUtils.obtenerImagen(2, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[2]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[2]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(3, maxLongitud);

                imagen = CruciUtils.obtenerImagen(3, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[3]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[3]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(5, maxLongitud);

                imagen = CruciUtils.obtenerImagen(4, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[4]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[4]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(6, maxLongitud);

                imagen = CruciUtils.obtenerImagen(5, inicios, imagenes);
                direccion = CruciUtils.direccionInicial(rutas[5]);
                posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[5]);
                maxLongitud = rutaMapa.size();
                limitesTexto.set(8, maxLongitud);

                lblPalabra1.setVisible(true);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(true);
                lblPalabra4.setVisible(true);
                lblPalabra5.setVisible(false);
                lblPalabra6.setVisible(true);
                lblPalabra7.setVisible(true);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(true);

                txtPalabra1.setVisible(true);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(true);
                txtPalabra4.setVisible(true);
                txtPalabra5.setVisible(false);
                txtPalabra6.setVisible(true);
                txtPalabra7.setVisible(true);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(true);

                jblLong1.setVisible(true);
                jblLong2.setVisible(false);
                jblLong3.setVisible(true);
                jblLong4.setVisible(true);
                jblLong5.setVisible(false);
                jblLong6.setVisible(true);
                jblLong7.setVisible(true);
                jblLong8.setVisible(false);
                jblLong9.setVisible(true);

                cleanData();
                break;
            }
            case 19: {
                String[] inicios = Parametros.modelo19Inicios.split(",");
                String[] imagenes = Parametros.modelo19Imagenes.split(",");
                String[] rutas = Parametros.modelo19Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                lblPalabra1.setVisible(false);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);

                txtPalabra1.setVisible(false);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(false);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);
                cleanData();
                break;
            }
            case 20: {
                String[] inicios = Parametros.modelo20Inicios.split(",");
                String[] imagenes = Parametros.modelo20Imagenes.split(",");
                String[] rutas = Parametros.modelo20Rutas.split(",");

                Imagen imagen = CruciUtils.obtenerImagen(0, inicios, imagenes);
                int direccion = CruciUtils.direccionInicial(rutas[0]);
                Posicion posicion = new Posicion();
                posicion.setX(imagen.getX());
                posicion.setY(imagen.getY());
                posicion.setDireccion(direccion);
                List<Posicion> rutaMapa = CruciUtils.obtenerRuta(posicion, rutas[0]);
                int maxLongitud = rutaMapa.size();
                limitesTexto.set(4, maxLongitud);

                lblPalabra1.setVisible(false);
                lblPalabra2.setVisible(false);
                lblPalabra3.setVisible(false);
                lblPalabra4.setVisible(false);
                lblPalabra5.setVisible(true);
                lblPalabra6.setVisible(false);
                lblPalabra7.setVisible(false);
                lblPalabra8.setVisible(false);
                lblPalabra9.setVisible(false);

                txtPalabra1.setVisible(false);
                txtPalabra2.setVisible(false);
                txtPalabra3.setVisible(false);
                txtPalabra4.setVisible(false);
                txtPalabra5.setVisible(true);
                txtPalabra6.setVisible(false);
                txtPalabra7.setVisible(false);
                txtPalabra8.setVisible(false);
                txtPalabra9.setVisible(false);

                jblLong1.setVisible(false);
                jblLong2.setVisible(false);
                jblLong3.setVisible(false);
                jblLong4.setVisible(false);
                jblLong5.setVisible(true);
                jblLong6.setVisible(false);
                jblLong7.setVisible(false);
                jblLong8.setVisible(false);
                jblLong9.setVisible(false);

                cleanData();
                break;
            }
        }
    }

    public String getPalabra(int imagen) {
        switch (imagen) {
            case 1: {
                return txtPalabra1.getText();
            }
            case 2: {
                return txtPalabra2.getText();
            }
            case 3: {
                return txtPalabra3.getText();
            }
            case 4: {
                return txtPalabra4.getText();
            }
            case 5: {
                return txtPalabra5.getText();
            }
            case 6: {
                return txtPalabra6.getText();
            }
            case 7: {
                return txtPalabra7.getText();
            }
            case 8: {
                return txtPalabra8.getText();
            }
            case 9: {
                return txtPalabra9.getText();
            }
        }
        return "";
    }

    private void cleanData() {
        jblLong1.setText("0");
        jblLong2.setText("0");
        jblLong3.setText("0");
        jblLong4.setText("0");
        jblLong5.setText("0");
        jblLong6.setText("0");
        jblLong7.setText("0");
        jblLong8.setText("0");
        jblLong9.setText("0");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGenerar = new javax.swing.JButton();
        txtSelect = new javax.swing.JLabel();
        panelModelos = new javax.swing.JScrollPane();
        modelosContainer = new javax.swing.JPanel();
        btnModelo01 = new javax.swing.JToggleButton();
        btnModelo02 = new javax.swing.JToggleButton();
        btnModelo03 = new javax.swing.JToggleButton();
        btnModelo04 = new javax.swing.JToggleButton();
        btnModelo05 = new javax.swing.JToggleButton();
        btnModelo06 = new javax.swing.JToggleButton();
        btnModelo09 = new javax.swing.JToggleButton();
        btnModelo07 = new javax.swing.JToggleButton();
        btnModelo08 = new javax.swing.JToggleButton();
        btnModelo11 = new javax.swing.JToggleButton();
        btnModelo10 = new javax.swing.JToggleButton();
        btnModelo13 = new javax.swing.JToggleButton();
        btnModelo12 = new javax.swing.JToggleButton();
        btnModelo14 = new javax.swing.JToggleButton();
        btnModelo15 = new javax.swing.JToggleButton();
        btnModelo16 = new javax.swing.JToggleButton();
        btnModelo17 = new javax.swing.JToggleButton();
        btnModelo18 = new javax.swing.JToggleButton();
        btnModelo19 = new javax.swing.JToggleButton();
        btnModelo20 = new javax.swing.JToggleButton();
        checkLetras = new javax.swing.JCheckBox();
        checkSimbolos = new javax.swing.JCheckBox();
        checkAyuda = new javax.swing.JCheckBox();
        checkLimpiar = new javax.swing.JCheckBox();
        Crucigrama_Titulo = new javax.swing.JLabel();
        contenedor = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblPalabra1 = new javax.swing.JLabel();
        lblPalabra2 = new javax.swing.JLabel();
        txtPalabra2 = new javax.swing.JTextField();
        lblPalabra3 = new javax.swing.JLabel();
        txtPalabra3 = new javax.swing.JTextField();
        txtPalabra4 = new javax.swing.JTextField();
        lblPalabra4 = new javax.swing.JLabel();
        lblPalabra5 = new javax.swing.JLabel();
        txtPalabra5 = new javax.swing.JTextField();
        txtPalabra6 = new javax.swing.JTextField();
        lblPalabra6 = new javax.swing.JLabel();
        lblPalabra7 = new javax.swing.JLabel();
        txtPalabra7 = new javax.swing.JTextField();
        txtPalabra8 = new javax.swing.JTextField();
        lblPalabra8 = new javax.swing.JLabel();
        lblPalabra9 = new javax.swing.JLabel();
        txtPalabra9 = new javax.swing.JTextField();
        jblLong1 = new javax.swing.JLabel();
        jblLong2 = new javax.swing.JLabel();
        jblLong3 = new javax.swing.JLabel();
        jblLong4 = new javax.swing.JLabel();
        jblLong5 = new javax.swing.JLabel();
        jblLong6 = new javax.swing.JLabel();
        jblLong7 = new javax.swing.JLabel();
        jblLong8 = new javax.swing.JLabel();
        jblLong9 = new javax.swing.JLabel();
        txtPalabra1 = new javax.swing.JTextField();
        menuMain = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuNuevo = new javax.swing.JMenuItem();
        menuExport = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSalir = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuPalabras = new javax.swing.JMenuItem();
        itemCargar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);

        btnGenerar.setText("Generar");
        btnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarActionPerformed(evt);
            }
        });

        txtSelect.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtSelect.setText("Seleccionar Modelo");

        panelModelos.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        panelModelos.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        panelModelos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnModelo01.setSelected(true);
        btnModelo01.setText("Modelo 1");
        btnModelo01.setContentAreaFilled(false);
        btnModelo01.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnModelo01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo01ActionPerformed(evt);
            }
        });

        btnModelo02.setText("Modelo 2");
        btnModelo02.setBorderPainted(false);
        btnModelo02.setContentAreaFilled(false);
        btnModelo02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo02ActionPerformed(evt);
            }
        });

        btnModelo03.setText("Modelo 3");
        btnModelo03.setBorderPainted(false);
        btnModelo03.setContentAreaFilled(false);
        btnModelo03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo03ActionPerformed(evt);
            }
        });

        btnModelo04.setText("Modelo 4");
        btnModelo04.setBorderPainted(false);
        btnModelo04.setContentAreaFilled(false);
        btnModelo04.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo04ActionPerformed(evt);
            }
        });

        btnModelo05.setText("Modelo 5");
        btnModelo05.setBorderPainted(false);
        btnModelo05.setContentAreaFilled(false);
        btnModelo05.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo05ActionPerformed(evt);
            }
        });

        btnModelo06.setText("Modelo 6");
        btnModelo06.setBorderPainted(false);
        btnModelo06.setContentAreaFilled(false);
        btnModelo06.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo06ActionPerformed(evt);
            }
        });

        btnModelo09.setText("Modelo 9");
        btnModelo09.setBorderPainted(false);
        btnModelo09.setContentAreaFilled(false);
        btnModelo09.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo09ActionPerformed(evt);
            }
        });

        btnModelo07.setText("Modelo 7");
        btnModelo07.setBorderPainted(false);
        btnModelo07.setContentAreaFilled(false);
        btnModelo07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo07ActionPerformed(evt);
            }
        });

        btnModelo08.setText("Modelo 8");
        btnModelo08.setBorderPainted(false);
        btnModelo08.setContentAreaFilled(false);
        btnModelo08.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo08ActionPerformed(evt);
            }
        });

        btnModelo11.setText("Modelo 11");
        btnModelo11.setBorderPainted(false);
        btnModelo11.setContentAreaFilled(false);
        btnModelo11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo11ActionPerformed(evt);
            }
        });

        btnModelo10.setText("Modelo 10");
        btnModelo10.setBorderPainted(false);
        btnModelo10.setContentAreaFilled(false);
        btnModelo10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo10ActionPerformed(evt);
            }
        });

        btnModelo13.setText("Modelo 13");
        btnModelo13.setBorderPainted(false);
        btnModelo13.setContentAreaFilled(false);
        btnModelo13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo13ActionPerformed(evt);
            }
        });

        btnModelo12.setText("Modelo 12");
        btnModelo12.setBorderPainted(false);
        btnModelo12.setContentAreaFilled(false);
        btnModelo12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo12ActionPerformed(evt);
            }
        });

        btnModelo14.setText("Modelo 14");
        btnModelo14.setBorderPainted(false);
        btnModelo14.setContentAreaFilled(false);
        btnModelo14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo14ActionPerformed(evt);
            }
        });

        btnModelo15.setText("Modelo 15");
        btnModelo15.setBorderPainted(false);
        btnModelo15.setContentAreaFilled(false);
        btnModelo15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo15ActionPerformed(evt);
            }
        });

        btnModelo16.setText("Modelo 16");
        btnModelo16.setBorderPainted(false);
        btnModelo16.setContentAreaFilled(false);
        btnModelo16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo16ActionPerformed(evt);
            }
        });

        btnModelo17.setText("Modelo 17");
        btnModelo17.setBorderPainted(false);
        btnModelo17.setContentAreaFilled(false);
        btnModelo17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo17ActionPerformed(evt);
            }
        });

        btnModelo18.setText("Modelo 18");
        btnModelo18.setBorderPainted(false);
        btnModelo18.setContentAreaFilled(false);
        btnModelo18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo18ActionPerformed(evt);
            }
        });

        btnModelo19.setText("Modelo 19");
        btnModelo19.setBorderPainted(false);
        btnModelo19.setContentAreaFilled(false);
        btnModelo19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo19ActionPerformed(evt);
            }
        });

        btnModelo20.setText("Modelo 20");
        btnModelo20.setBorderPainted(false);
        btnModelo20.setContentAreaFilled(false);
        btnModelo20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModelo20ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout modelosContainerLayout = new javax.swing.GroupLayout(modelosContainer);
        modelosContainer.setLayout(modelosContainerLayout);
        modelosContainerLayout.setHorizontalGroup(
            modelosContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(modelosContainerLayout.createSequentialGroup()
                .addGroup(modelosContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(modelosContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnModelo04, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo03, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo01, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo05, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo06, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo09, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo07, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo08, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModelo02, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnModelo17, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModelo18, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModelo19, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModelo20, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 40, Short.MAX_VALUE))
        );
        modelosContainerLayout.setVerticalGroup(
            modelosContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(modelosContainerLayout.createSequentialGroup()
                .addComponent(btnModelo01)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo02)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo03)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo04)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo05)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo06)
                .addGap(5, 5, 5)
                .addComponent(btnModelo07)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo08)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo09)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo11)
                .addGap(9, 9, 9)
                .addComponent(btnModelo12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo20)
                .addContainerGap(97, Short.MAX_VALUE))
        );

        panelModelos.setViewportView(modelosContainer);

        checkLetras.setSelected(true);
        checkLetras.setText("Respuesta");
        checkLetras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkLetrasActionPerformed(evt);
            }
        });

        checkSimbolos.setSelected(true);
        checkSimbolos.setText("Simbolos");
        checkSimbolos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkSimbolosActionPerformed(evt);
            }
        });

        checkAyuda.setSelected(true);
        checkAyuda.setText("Guia");
        checkAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkAyudaActionPerformed(evt);
            }
        });

        checkLimpiar.setSelected(true);
        checkLimpiar.setText("Reutilizar Palabras");
        checkLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkLimpiarActionPerformed(evt);
            }
        });

        Crucigrama_Titulo.setText(" ");

        contenedor.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout contenedorLayout = new javax.swing.GroupLayout(contenedor);
        contenedor.setLayout(contenedorLayout);
        contenedorLayout.setHorizontalGroup(
            contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 624, Short.MAX_VALUE)
        );
        contenedorLayout.setVerticalGroup(
            contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        lblPalabra1.setText("ARRIBA IZQUIERDA:");

        lblPalabra2.setText("ARRIBA CENTRO:");

        txtPalabra2.setToolTipText("ARRIBA DERECHA");
        txtPalabra2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra2KeyReleased(evt);
            }
        });

        lblPalabra3.setText("ARRIBA DERECHA:");

        txtPalabra3.setToolTipText("ARRIBA DERECHA");
        txtPalabra3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra3KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra3KeyReleased(evt);
            }
        });

        txtPalabra4.setToolTipText("ARRIBA DERECHA");
        txtPalabra4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra4KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra4KeyReleased(evt);
            }
        });

        lblPalabra4.setText("MEDIO IZQUIERDA:");

        lblPalabra5.setText("MEDIO CENTRO:");

        txtPalabra5.setToolTipText("ARRIBA DERECHA");
        txtPalabra5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra5KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra5KeyReleased(evt);
            }
        });

        txtPalabra6.setToolTipText("ARRIBA DERECHA");
        txtPalabra6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra6KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra6KeyReleased(evt);
            }
        });

        lblPalabra6.setText("MEDIO DERECHA:");

        lblPalabra7.setText("ABAJO IZQUIERDA:");

        txtPalabra7.setToolTipText("ARRIBA DERECHA");
        txtPalabra7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra7KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra7KeyReleased(evt);
            }
        });

        txtPalabra8.setToolTipText("ARRIBA DERECHA");
        txtPalabra8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra8KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra8KeyReleased(evt);
            }
        });

        lblPalabra8.setText("ABAJO CENTRO:");

        lblPalabra9.setText("ABAJO DERECHA:");

        txtPalabra9.setToolTipText("ARRIBA DERECHA");
        txtPalabra9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra9KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra9KeyReleased(evt);
            }
        });

        jblLong1.setText("0");

        jblLong2.setText("0");

        jblLong3.setText("0");

        jblLong4.setText("0");

        jblLong5.setText("0");

        jblLong6.setText("0");

        jblLong7.setText("0");

        jblLong8.setText("0");

        jblLong9.setText("0");

        txtPalabra1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPalabra1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPalabra1KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPalabra8, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtPalabra7)
                    .addComponent(txtPalabra6)
                    .addComponent(txtPalabra3)
                    .addComponent(txtPalabra2)
                    .addComponent(txtPalabra4)
                    .addComponent(txtPalabra5)
                    .addComponent(txtPalabra9)
                    .addComponent(txtPalabra1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra4)
                                .addGap(16, 16, 16)
                                .addComponent(jblLong4))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jblLong9))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jblLong8))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jblLong6))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra5)
                                .addGap(18, 18, 18)
                                .addComponent(jblLong5))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jblLong3))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra2)
                                .addGap(18, 18, 18)
                                .addComponent(jblLong2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra7, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jblLong7))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPalabra1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jblLong1)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra1)
                    .addComponent(jblLong1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtPalabra1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra2)
                    .addComponent(jblLong2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra3)
                    .addComponent(jblLong3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra4)
                    .addComponent(jblLong4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra5)
                    .addComponent(jblLong5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra6)
                    .addComponent(jblLong6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra7)
                    .addComponent(jblLong7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra8)
                    .addComponent(jblLong8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPalabra9)
                    .addComponent(jblLong9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPalabra9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        menuMain.setMaximumSize(new java.awt.Dimension(274, 42768));

        jMenu1.setText("Crucigrama");

        menuNuevo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        menuNuevo.setText("Nuevo");
        menuNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNuevoActionPerformed(evt);
            }
        });
        jMenu1.add(menuNuevo);

        menuExport.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        menuExport.setText("Exportar");
        menuExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExportActionPerformed(evt);
            }
        });
        jMenu1.add(menuExport);
        jMenu1.add(jSeparator1);

        menuSalir.setText("Salir");
        menuSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalirActionPerformed(evt);
            }
        });
        jMenu1.add(menuSalir);

        menuMain.add(jMenu1);

        jMenu2.setText("Palabras");

        menuPalabras.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        menuPalabras.setText("Gestionar Palabras");
        menuPalabras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPalabrasActionPerformed(evt);
            }
        });
        jMenu2.add(menuPalabras);

        itemCargar.setText("Cargar Archivo de Palabras");
        itemCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCargarActionPerformed(evt);
            }
        });
        jMenu2.add(itemCargar);

        menuMain.add(jMenu2);

        setJMenuBar(menuMain);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkLetras)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(checkSimbolos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(checkAyuda)
                        .addGap(67, 67, 67)
                        .addComponent(Crucigrama_Titulo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 499, Short.MAX_VALUE)
                        .addComponent(checkLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGenerar, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(contenedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelModelos, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSelect)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelModelos, javax.swing.GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE))
                            .addComponent(contenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGenerar)
                    .addComponent(checkLetras)
                    .addComponent(checkSimbolos)
                    .addComponent(checkAyuda)
                    .addComponent(checkLimpiar)
                    .addComponent(Crucigrama_Titulo))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed
        // TODO add your handling code here:
        try {
            contenedor.getGraphics().clearRect(0, 0, ANCHO, ALTO);
            generar();
            nuevo = false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnGenerarActionPerformed

    private void menuNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuNuevoActionPerformed
        // TODO add your handling code here:
        nuevo = true;
        super.repaint();
    }//GEN-LAST:event_menuNuevoActionPerformed

    private void menuSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_menuSalirActionPerformed

    private void btnModelo01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo01ActionPerformed
        // TODO add your handling code here:
        plantilla = 1;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo01ActionPerformed

    private void btnModelo02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo02ActionPerformed
        // TODO add your handling code here:
        plantilla = 2;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo02ActionPerformed

    private void btnModelo03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo03ActionPerformed
        // TODO add your handling code here:
        plantilla = 3;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo03ActionPerformed

    private void btnModelo04ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo04ActionPerformed
        // TODO add your handling code here:
        plantilla = 4;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo04ActionPerformed

    private void btnModelo05ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo05ActionPerformed
        // TODO add your handling code here:
        plantilla = 5;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo05ActionPerformed

    private void btnModelo06ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo06ActionPerformed
        // TODO add your handling code here:
        plantilla = 6;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo06ActionPerformed

    private void btnModelo07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo07ActionPerformed
        // TODO add your handling code here:
        plantilla = 7;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo07ActionPerformed

    private void btnModelo08ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo08ActionPerformed
        // TODO add your handling code here:
        plantilla = 8;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo08ActionPerformed

    private void btnModelo09ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo09ActionPerformed
        // TODO add your handling code here:
        plantilla = 9;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo09ActionPerformed

    private void btnModelo10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo10ActionPerformed
        // TODO add your handling code here:
        plantilla = 10;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo10ActionPerformed

    private void btnModelo11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo11ActionPerformed
        // TODO add your handling code here:
        plantilla = 11;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo11ActionPerformed

    private void btnModelo12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo12ActionPerformed
        // TODO add your handling code here:
        plantilla = 12;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo12ActionPerformed

    private void btnModelo13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo13ActionPerformed
        // TODO add your handling code here:
        plantilla = 13;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo13ActionPerformed

    private void btnModelo14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo14ActionPerformed
        // TODO add your handling code here:
        plantilla = 14;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo14ActionPerformed

    private void btnModelo15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo15ActionPerformed
        // TODO add your handling code here:
        plantilla = 15;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo15ActionPerformed

    private void btnModelo16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo16ActionPerformed
        // TODO add your handling code here:
        plantilla = 16;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo16ActionPerformed

    private void btnModelo17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo17ActionPerformed
        // TODO add your handling code here:
        plantilla = 17;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo17ActionPerformed

    private void btnModelo18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo18ActionPerformed
        // TODO add your handling code here:
        plantilla = 18;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo18ActionPerformed

    private void btnModelo19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo19ActionPerformed
        // TODO add your handling code here:
        plantilla = 19;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo19ActionPerformed

    private void btnModelo20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModelo20ActionPerformed
        // TODO add your handling code here:
        plantilla = 20;
        Crucigrama_Titulo.setText("Crucigrama - Modelo " + plantilla);
        nuevo = true;
        super.repaint();
        actualizarPalabras();
        actualizarBotones();
    }//GEN-LAST:event_btnModelo20ActionPerformed

    private void checkLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkLimpiarActionPerformed
        // TODO add your handling code here:
        limpiar = checkLimpiar.isSelected();
    }//GEN-LAST:event_checkLimpiarActionPerformed

    private void checkLetrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkLetrasActionPerformed
        // TODO add your handling code here:
        mostrarLetras = checkLetras.isSelected();
        super.repaint();
    }//GEN-LAST:event_checkLetrasActionPerformed

    private void checkSimbolosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkSimbolosActionPerformed
        // TODO add your handling code here:
        mostrarSimbolos = checkSimbolos.isSelected();
        super.repaint();
    }//GEN-LAST:event_checkSimbolosActionPerformed

    private void checkAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkAyudaActionPerformed
        // TODO add your handling code here:
        mostrarGuia = checkAyuda.isSelected();
        super.repaint();
    }//GEN-LAST:event_checkAyudaActionPerformed

    private void menuPalabrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPalabrasActionPerformed
        // TODO add your handling code here:
        principal.setVisible(false);
        framePalabras.setVisible(true);
    }//GEN-LAST:event_menuPalabrasActionPerformed

    private void itemCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCargarActionPerformed
        // TODO add your handling code here:
        cargarArchivo();
    }//GEN-LAST:event_itemCargarActionPerformed

    private void menuExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExportActionPerformed
        // TODO add your handling code here:
        if (!nuevo && crucigrama != null) {
            try {
                JFileChooser fileChooser = new JFileChooser();
                FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter("Archivos PDF (*.pdf)", "pdf");
                fileChooser.addChoosableFileFilter(xmlFilter);
                fileChooser.setFileFilter(xmlFilter);
                fileChooser.setDialogTitle("Guardar Crucigrama como PDF");
                File fileToSave = null;
                while (fileToSave == null || !fileToSave.getAbsolutePath().contains(".pdf")) {
                    int userSelection = fileChooser.showSaveDialog(this);
                    if (userSelection == JFileChooser.APPROVE_OPTION) {
                        fileToSave = fileChooser.getSelectedFile();
                        if (!fileToSave.getName().toLowerCase().contains(".pdf")) {
                            fileToSave = new File(fileToSave.getAbsolutePath() + ".pdf");
                        }
                    } else {
                        return;
                    }
                }

                final String filePath = fileToSave.getAbsolutePath();
                Runnable r = new Runnable() {
                    public void run() {
                        generatePdf(filePath);
                    }
                };
                new Thread(r).start();
                waitingDialog.setVisible(true);
            } catch (Exception ex) {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_menuExportActionPerformed

    private void txtPalabra2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra2KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(1);
        int result = max - txtPalabra2.getText().replace(" ", "").length();
        jblLong2.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra2.setText(txtPalabra2.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra2.getText().replace(" ", "").length() > max) {
                txtPalabra2.setText(txtPalabra2.getText().substring(0, txtPalabra2.getText().length() - 1));
            }
        }

    }//GEN-LAST:event_txtPalabra2KeyReleased

    private void txtPalabra3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra3KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(2);
        int result = max - txtPalabra3.getText().replace(" ", "").length();
        jblLong3.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra3.setText(txtPalabra3.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra3.getText().replace(" ", "").length() > max) {
                txtPalabra3.setText(txtPalabra3.getText().substring(0, txtPalabra3.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra3KeyReleased

    private void txtPalabra4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra4KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(3);
        int result = max - txtPalabra4.getText().replace(" ", "").length();
        jblLong4.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra4.setText(txtPalabra4.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra4.getText().replace(" ", "").length() > max) {
                txtPalabra4.setText(txtPalabra4.getText().substring(0, txtPalabra4.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra4KeyReleased

    private void txtPalabra5KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra5KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(4);
        int result = max - txtPalabra5.getText().replace(" ", "").length();
        jblLong5.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra5.setText(txtPalabra5.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra5.getText().replace(" ", "").length() > max) {
                txtPalabra5.setText(txtPalabra5.getText().substring(0, txtPalabra5.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra5KeyReleased

    private void txtPalabra6KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra6KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(5);
        int result = max - txtPalabra6.getText().replace(" ", "").length();
        jblLong6.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra6.setText(txtPalabra6.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra6.getText().replace(" ", "").length() > max) {
                txtPalabra6.setText(txtPalabra6.getText().substring(0, txtPalabra6.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra6KeyReleased

    private void txtPalabra7KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra7KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(6);
        int result = max - txtPalabra7.getText().replace(" ", "").length();
        jblLong7.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra7.setText(txtPalabra7.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra7.getText().replace(" ", "").length() > max) {
                txtPalabra7.setText(txtPalabra7.getText().substring(0, txtPalabra7.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra7KeyReleased

    private void txtPalabra8KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra8KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(7);
        int result = max - txtPalabra8.getText().replace(" ", "").length();
        jblLong8.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra8.setText(txtPalabra8.getText().replaceAll(PATTERN, ""));
                return; 
            }

            if (txtPalabra8.getText().replace(" ", "").length() > max) {
                txtPalabra8.setText(txtPalabra8.getText().substring(0, txtPalabra8.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra8KeyReleased

    private void txtPalabra9KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra9KeyReleased
        // TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(8);
        int result = max - txtPalabra9.getText().replace(" ", "").length();
        jblLong9.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127) || (in == 49) || (in == 65535) || (in == 8) || (in == 127)
                    || // es espacio
                    (in > 96 && in < 123)
                    || // es A-Z
                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra9.setText(txtPalabra9.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra9.getText().replace(" ", "").length() > max) {
                txtPalabra9.setText(txtPalabra9.getText().substring(0, txtPalabra9.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra9KeyReleased

    private void txtPalabra2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra2KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(1);
        int result = max - txtPalabra2.getText().replace(" ", "").length();
        jblLong2.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra2KeyPressed

    private void txtPalabra3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra3KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(2);
        int result = max - txtPalabra3.getText().replace(" ", "").length();
        jblLong3.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra3KeyPressed

    private void txtPalabra4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra4KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(3);
        int result = max - txtPalabra4.getText().replace(" ", "").length();
        jblLong4.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra4KeyPressed

    private void txtPalabra5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra5KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(4);
        int result = max - txtPalabra5.getText().replace(" ", "").length();
        jblLong5.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra5KeyPressed

    private void txtPalabra6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra6KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(5);
        int result = max - txtPalabra6.getText().replace(" ", "").length();
        jblLong6.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra6KeyPressed

    private void txtPalabra7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra7KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(6);
        int result = max - txtPalabra7.getText().replace(" ", "").length();
        jblLong7.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra7KeyPressed

    private void txtPalabra8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra8KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(7);
        int result = max - txtPalabra8.getText().replace(" ", "").length();
        jblLong8.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra8KeyPressed

    private void txtPalabra9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra9KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(8);
        int result = max - txtPalabra9.getText().replace(" ", "").length();
        jblLong9.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra9KeyPressed

    private void txtPalabra1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra1KeyPressed
        // TODO add your handling code here:
        int max = limitesTexto.get(0);
        int result = max - txtPalabra1.getText().replace(" ", "").length();
        jblLong1.setText(String.valueOf(result));
        if (result == 0) {
            return;
        }
    }//GEN-LAST:event_txtPalabra1KeyPressed

    private void txtPalabra1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPalabra1KeyReleased
// TODO add your handling code here:
        int in = evt.getKeyChar();
        int max = limitesTexto.get(0);
        int result = max - txtPalabra1.getText().replace(" ", "").length();
        jblLong1.setText(String.valueOf(result));

        if (result == 0) {
            return;
        } else {
            System.out.println("In: " + in);
            boolean valido = (in == 32) || (in == 49) || (in == 65535) || (in == 8) || (in == 127) || (in > 96 && in < 123);
//                    || // es A-Z
//                    (in > 64 && in < 91);       // es a-z
            if (!valido) {
                txtPalabra1.setText(txtPalabra1.getText().replaceAll(PATTERN, ""));
                return;
            }

            if (txtPalabra1.getText().replace(" ", "").length() > max) {
                txtPalabra1.setText(txtPalabra1.getText().substring(0, txtPalabra1.getText().length() - 1));
            }
        }
    }//GEN-LAST:event_txtPalabra1KeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {

                    principal = new Principal();
                    principal.setSize(ANCHO, ALTO);
                    principal.setLocationRelativeTo(null);
                    principal.setVisible(true);
                    framePalabras = new FramePalabras(principal);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void generar() {
        try {
            //GENERAR CRUCIGRAMA
            Runnable r = new Runnable() {
                public void run() {
                    generateBackground();
                    principal.repaint();
                }
            };
            new Thread(r).start();
            waitingDialog.setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "No exiten palabras disponibles.\nCargue un archivo de palabras desde el menú \nPalabras > Cargar archivo de palabras.");
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.black);
        g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
        if (nuevo) {
            dibujarVacio(g);
            return;
        }
        dibujarLetras(g);
        if (mostrarSimbolos && simbolos != null) {
            dibujarSimbolos(g);
        }
        if (mostrarGuia && guias != null) {
            dibujarGuias(g);
        }
    }

    private void pdfSimbolos(Map parameterMap) {
        Iterator<Map.Entry<Posicion, List<Simbolo>>> map = simbolos.entrySet().iterator();
        while (map.hasNext()) {
            Map.Entry<Posicion, List<Simbolo>> simb = map.next();
            Posicion posicion = simb.getKey();
            List<Simbolo> simbolos = simb.getValue();
            if (simbolos != null) {
                for (Simbolo simbolo : simbolos) {
                    int i = posicion.getY();
                    int j = posicion.getX();
                    String posicionStr = "";
                    boolean last = false;
                    boolean space = (simbolo.getIdSimbolo() == Constantes.SIMBOLO_ESPACIO);
                    switch (simbolo.getPosicionDibujar()) {
                        case Constantes.DIBUJAR_ABAJ_DER: {
                            posicionStr = "dr";
                            last = true;
                            break;
                        }
                        case Constantes.DIBUJAR_ABAJ_IZQ: {
                            posicionStr = "dl";
                            last = true;
                            break;
                        }
                        case Constantes.DIBUJAR_ARRI_DER: {
                            posicionStr = "ur";
                            last = true;
                            break;
                        }
                        case Constantes.DIBUJAR_ARRI_IZQ: {
                            posicionStr = "ul";
                            last = true;
                            break;
                        }
                        case Constantes.DIBUJAR_ARRIBA: {
                            posicionStr = "lu";
                            if (space) {
                                posicionStr = "su";
                            }
                            break;
                        }
                        case Constantes.DIBUJAR_DERECHA: {
                            posicionStr = "lr";
                            if (space) {
                                posicionStr = "sr";
                            }
                            break;
                        }
                        case Constantes.DIBUJAR_ABAJO: {
                            posicionStr = "ld";
                            if (space) {
                                posicionStr = "sd";
                            }
                            break;
                        }
                        case Constantes.DIBUJAR_IZQUIERDA: {
                            posicionStr = "ll";
                            if (space) {
                                posicionStr = "sl";
                            }
                            break;
                        }
                    }
                    if (!posicionStr.isEmpty()) {
                        String param = i + "-" + j;
//                        parameterMap.put("su-" + param, "1");
//                        parameterMap.put("sr-" + param, "1");
//                        parameterMap.put("sd-" + param, "1");
//                        parameterMap.put("sl-" + param, "1");
                        param = space || !last ? (posicionStr + "-" + param) : ("s-" + param + "-" + posicionStr);
                        parameterMap.put(param, "1");
                        System.out.println("PARAM: " + param);
                    }
                }
            }
        }
//        SHOW ALL ARROWS
//        for (int i = 0; i < 18; i++) {
//            for (int j = 0; j < 15; j++) {
//                parameterMap.put("sl-" + i + "-" + j, "1");
//                parameterMap.put("sr-" + i + "-" + j, "1");
//                parameterMap.put("sd-" + i + "-" + j, "1");
//                parameterMap.put("su-" + i + "-" + j, "1");
//            }
//        }
    }

    private Map pdfLetras(Map parameterMap) {
        int altoCrucigrama = crucigrama.length;
        int anchoCrucigrama = crucigrama[0].length;
        for (int i = 0; i < altoCrucigrama; i++) {
            for (int j = 0; j < anchoCrucigrama; j++) {
                String letra = crucigrama[i][j];
                boolean show = false;
                if (letra.equals(".")) {
                    letra = "";
                    show = true;
                }
                if (letra.equals("#")) {
                    letra = "#";
                    show = true;
                }
                if (letra.equals("@")) {
                    letra = "@";
                    show = true;
                }
                if (!show && !mostrarLetras) {
                    letra = "";
                }
                letra = letra == null || letra.equalsIgnoreCase("null") ? "" : letra;
                parameterMap.put("w-" + i + "-" + j + "", letra);
            }
        }

        if (mostrarGuia) {
            Iterator<Map.Entry<Posicion, List<Leyenda>>> map = guias.entrySet().iterator();
            while (map.hasNext()) {
                Map.Entry<Posicion, List<Leyenda>> guia = map.next();
                Posicion posicion = guia.getKey();
                List<Leyenda> leyendas = guia.getValue();
                if (leyendas != null) {
                    for (Leyenda leyenda : leyendas) {
                        String ayuda = leyenda.getLeyenda().getDescripcion();
                        switch (leyenda.getPosicionDibujar()) {
                            case Constantes.DIBUJAR_ARRIBA: {
                                parameterMap.put("h-" + posicion.getY() + "-" + posicion.getX() + "-u", ayuda);
                                break;
                            }
                            case Constantes.DIBUJAR_IZQUIERDA: {
                                parameterMap.put("h-" + posicion.getY() + "-" + posicion.getX() + "-l", ayuda);
                                break;
                            }
                            case Constantes.DIBUJAR_DERECHA: {
                                parameterMap.put("h-" + posicion.getY() + "-" + posicion.getX() + "-r", ayuda);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return parameterMap;
    }

    private void dibujarLetras(Graphics g) {
        if (crucigrama.length > 0) {
            int altoCrucigrama = crucigrama.length;
            int anchoCrucigrama = crucigrama[0].length;

            for (int i = 0; i < altoCrucigrama; i++) {
                for (int j = 0; j < anchoCrucigrama; j++) {
                    boolean respuesta = mostrarLetras;
                    String letra = crucigrama[i][j];
                    if (letra.equals(".")) {
                        letra = "";
                    }
                    if (letra.equals("#")) {
                        letra = "#";
                        respuesta = true;
                    }
                    if (letra.equals("@")) {
                        letra = "@";
                        respuesta = true;
                    }
                    dibujarLetra(j, i, letra, respuesta, g);
                }
            }
        }
    }

    private void dibujarSimbolos(Graphics g) {
        Iterator<Map.Entry<Posicion, List<Simbolo>>> map = simbolos.entrySet().iterator();
        while (map.hasNext()) {
            Map.Entry<Posicion, List<Simbolo>> simb = map.next();
            Posicion posicion = simb.getKey();
            List<Simbolo> simbolos = simb.getValue();
            if (simbolos != null) {
                for (Simbolo simbolo : simbolos) {
                    dibujarSimbolo(posicion, simbolo, g);
                }
            }
        }
    }

    private void dibujarGuias(Graphics g) {
        Iterator<Map.Entry<Posicion, List<Leyenda>>> map = guias.entrySet().iterator();
        while (map.hasNext()) {
            Map.Entry<Posicion, List<Leyenda>> guia = map.next();
            Posicion posicion = guia.getKey();
            List<Leyenda> leyendas = guia.getValue();
            if (leyendas != null) {
                for (Leyenda leyenda : leyendas) {
                    dibujarGuia(posicion, leyenda, g);
                }
            }
        }
    }

    private void dibujarGuia(Posicion posicion, Leyenda simbolo, Graphics g) {
        Posicion auxPosicion = new Posicion(posicion.getX() + 3, posicion.getY() + 3);
        String palabra = simbolo.getLeyenda().getDescripcion();
        switch (simbolo.getPosicionDibujar()) {
            case Constantes.DIBUJAR_IZQUIERDA: {
                dibujarGuiaIzquierda(auxPosicion.getX(), auxPosicion.getY(), palabra, g);
                break;
            }
            case Constantes.DIBUJAR_ARRIBA: {
                dibujarGuiaArriba(auxPosicion.getX(), auxPosicion.getY(), palabra, g);
                break;
            }
            case Constantes.DIBUJAR_DERECHA: {
                dibujarGuiaDerecha(auxPosicion.getX(), auxPosicion.getY(), palabra, g);
                break;
            }
            case Constantes.DIBUJAR_ABAJO: {
                dibujarGuiaAbajo(auxPosicion.getX(), auxPosicion.getY(), palabra, g);
                break;
            }
        }
    }

    private void dibujarSimbolo(Posicion posicion, Simbolo simbolo, Graphics g) {
        Posicion auxPosicion = new Posicion(posicion.getX() + 2, posicion.getY() + 2);
        switch (simbolo.getIdSimbolo()) {
            case Constantes.SIMBOLO_INICIAL: {
                switch (simbolo.getPosicionDibujar()) {
                    case Constantes.DIBUJAR_DERECHA: {
                        dibujarInicialDerecha(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_ABAJO: {
                        dibujarInicialAbajo(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_IZQUIERDA: {
                        dibujarInicialIzquierda(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_ARRIBA: {
                        dibujarInicialArriba(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                }
                break;
            }
            case Constantes.SIMBOLO_FINAL: {
                switch (simbolo.getPosicionDibujar()) {
                    case Constantes.DIBUJAR_DERECHA: {
                        dibujarFinalDerecha(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_ABAJO: {
                        dibujarFinalAbajo(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_IZQUIERDA: {
                        dibujarFinalIzquierda(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_ARRIBA: {
                        dibujarFinalArriba(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                }
                break;
            }
            case Constantes.SIMBOLO_ESPACIO: {
                switch (simbolo.getPosicionDibujar()) {
                    case Constantes.DIBUJAR_DERECHA: {
                        dibujarEspacioDerecha(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_ABAJO: {
                        dibujarEspacioAbajo(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_IZQUIERDA: {
                        dibujarEspacioIzquierda(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                    case Constantes.DIBUJAR_ARRIBA: {
                        dibujarEspacioArriba(auxPosicion.getX(), auxPosicion.getY(), g);
                        break;
                    }
                }
                break;
            }
            case Constantes.SIMBOLO_ABAJ_IZQ: {/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                dibujarAbajoIzquierda(auxPosicion.getX(), auxPosicion.getY(), g);
                break;
            }
            case Constantes.SIMBOLO_ABAJ_DER: {/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                dibujarAbajoDerecha(auxPosicion.getX(), auxPosicion.getY(), g);
                break;
            }
            case Constantes.SIMBOLO_ARRI_DER: {/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                dibujarArribaDerecha(auxPosicion.getX(), auxPosicion.getY(), g);
                break;
            }
            case Constantes.SIMBOLO_ARRI_IZQ: {/*MODIFICAR PARA SABER HACIA QUE LADO APUNTA LA FLECHA*/
                dibujarArribaIzquierda(auxPosicion.getX(), auxPosicion.getY(), g);
                break;
            }
        }
    }

    private void cargarArchivo() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Palabras", "txt", "csv");
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(new java.io.File("."));
        int returnVal = chooser.showOpenDialog(principal);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                System.out.println("Inicializando Base de Datos con " + chooser.getSelectedFile().getAbsolutePath());
                CruciUtils.cargarBD(chooser.getSelectedFile().getAbsolutePath());
                System.out.println("Base de datos actualizada.");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Crucigrama_Titulo;
    private javax.swing.JButton btnGenerar;
    private javax.swing.JToggleButton btnModelo01;
    private javax.swing.JToggleButton btnModelo02;
    private javax.swing.JToggleButton btnModelo03;
    private javax.swing.JToggleButton btnModelo04;
    private javax.swing.JToggleButton btnModelo05;
    private javax.swing.JToggleButton btnModelo06;
    private javax.swing.JToggleButton btnModelo07;
    private javax.swing.JToggleButton btnModelo08;
    private javax.swing.JToggleButton btnModelo09;
    private javax.swing.JToggleButton btnModelo10;
    private javax.swing.JToggleButton btnModelo11;
    private javax.swing.JToggleButton btnModelo12;
    private javax.swing.JToggleButton btnModelo13;
    private javax.swing.JToggleButton btnModelo14;
    private javax.swing.JToggleButton btnModelo15;
    private javax.swing.JToggleButton btnModelo16;
    private javax.swing.JToggleButton btnModelo17;
    private javax.swing.JToggleButton btnModelo18;
    private javax.swing.JToggleButton btnModelo19;
    private javax.swing.JToggleButton btnModelo20;
    private javax.swing.JCheckBox checkAyuda;
    private javax.swing.JCheckBox checkLetras;
    private javax.swing.JCheckBox checkLimpiar;
    private javax.swing.JCheckBox checkSimbolos;
    private javax.swing.JPanel contenedor;
    private javax.swing.JMenuItem itemCargar;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel jblLong1;
    private javax.swing.JLabel jblLong2;
    private javax.swing.JLabel jblLong3;
    private javax.swing.JLabel jblLong4;
    private javax.swing.JLabel jblLong5;
    private javax.swing.JLabel jblLong6;
    private javax.swing.JLabel jblLong7;
    private javax.swing.JLabel jblLong8;
    private javax.swing.JLabel jblLong9;
    private javax.swing.JLabel lblPalabra1;
    private javax.swing.JLabel lblPalabra2;
    private javax.swing.JLabel lblPalabra3;
    private javax.swing.JLabel lblPalabra4;
    private javax.swing.JLabel lblPalabra5;
    private javax.swing.JLabel lblPalabra6;
    private javax.swing.JLabel lblPalabra7;
    private javax.swing.JLabel lblPalabra8;
    private javax.swing.JLabel lblPalabra9;
    private javax.swing.JMenuItem menuExport;
    private javax.swing.JMenuBar menuMain;
    private javax.swing.JMenuItem menuNuevo;
    private javax.swing.JMenuItem menuPalabras;
    private javax.swing.JMenuItem menuSalir;
    private javax.swing.JPanel modelosContainer;
    private javax.swing.JScrollPane panelModelos;
    private javax.swing.JTextField txtPalabra1;
    private javax.swing.JTextField txtPalabra2;
    private javax.swing.JTextField txtPalabra3;
    private javax.swing.JTextField txtPalabra4;
    private javax.swing.JTextField txtPalabra5;
    private javax.swing.JTextField txtPalabra6;
    private javax.swing.JTextField txtPalabra7;
    private javax.swing.JTextField txtPalabra8;
    private javax.swing.JTextField txtPalabra9;
    private javax.swing.JLabel txtSelect;
    // End of variables declaration//GEN-END:variables

    int relativoAlto = 30;
    int relativoAncho = 30;

    private void dibujarModelo(int i, int j, String letra, Graphics g) {
        i += 2;
        j += 2;
        g.drawRect(i * relativoAncho, j * relativoAlto, relativoAlto, relativoAlto);
        g.drawString(letra, (i * relativoAncho) + (int) (relativoAncho * 0.3), (j * relativoAlto) + (int) (relativoAlto * 0.8));
    }

    private void dibujarLetra(int i, int j, String letra, boolean respuesta, Graphics g) {
        i += 2;
        j += 2;
        g.drawRect(i * relativoAncho, j * relativoAlto, relativoAlto, relativoAlto);
        if (respuesta) {
            g.drawString(letra, (i * relativoAncho) + (int) (relativoAncho * 0.3), (j * relativoAlto) + (int) (relativoAlto * 0.8));
        }
    }

    private void dibujarFinalDerecha(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho) + (relativoAncho), (j * relativoAlto), (i * relativoAncho) + (relativoAncho), (j * relativoAlto) + (relativoAlto));
        g2.setStroke(auxStroke);
    }

    private void dibujarFinalAbajo(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho), (j * relativoAlto) + (relativoAlto), (i * relativoAncho) + (relativoAncho), (j * relativoAlto) + (relativoAlto));
        g2.setStroke(auxStroke);
    }

    private void dibujarFinalIzquierda(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho), (j * relativoAlto), (i * relativoAncho), (j * relativoAlto) + (relativoAlto));
        g2.setStroke(auxStroke);
    }

    private void dibujarFinalArriba(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho), (j * relativoAlto), (i * relativoAncho) + (relativoAncho), (j * relativoAlto));
        g2.setStroke(auxStroke);
    }

    private void dibujarInicialDerecha(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        char c = 8592; // APUNTAR IZQUIERDA
        g.drawString(String.valueOf(c), (i * relativoAncho) + (int) (relativoAncho * 0.2), (j * relativoAlto) + (int) (relativoAlto * 1.01));
        g.setFont(aux);
    }

    private void dibujarInicialAbajo(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        char c = 8593; // APUNTAR ARRIBA
        g.drawString(String.valueOf(c), (i * relativoAncho) + (int) (relativoAncho * 0.7), (j * relativoAlto) + (int) (relativoAncho * 0.5));
        g.setFont(aux);
    }

    private void dibujarInicialIzquierda(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        char c = 8594; // APUNTAR DERECHA
        g.drawString(String.valueOf(c), (i * relativoAncho) + (int) (relativoAncho * 0.3), (j * relativoAlto) + (int) (relativoAncho * 0.4));
        g.setFont(aux);
    }

    private void dibujarInicialArriba(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        int altoSimbolo = 30;
        char c = 8595; // APUNTAR ABAJO
        g.drawString(String.valueOf(c), (i * relativoAncho) + (int) (relativoAncho * 0.7), (j * relativoAlto) + altoSimbolo);
        g.setFont(aux);
    }

    private void dibujarEspacioDerecha(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho) + (relativoAncho - (relativoAncho / 8)), (j * relativoAlto) + (relativoAlto / 2), (i * relativoAncho) + (relativoAncho + (relativoAncho / 8)), (j * relativoAlto) + (relativoAlto / 2));
        g2.setStroke(auxStroke);
    }

    private void dibujarEspacioAbajo(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho) + (relativoAncho / 2), (j * relativoAlto) + (relativoAlto - (relativoAlto / 8)), (i * relativoAncho) + (relativoAncho / 2), (j * relativoAlto) + (relativoAlto + (relativoAlto / 8)));
        g2.setStroke(auxStroke);
    }

    private void dibujarEspacioIzquierda(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho) - (relativoAncho / 8), (j * relativoAlto) + (relativoAlto / 2), (i * relativoAncho) + (relativoAncho / 8), (j * relativoAlto) + (relativoAlto / 2));
        g2.setStroke(auxStroke);
    }

    private void dibujarEspacioArriba(int i, int j, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke stroke = new BasicStroke(3F);
        Stroke auxStroke = g2.getStroke();
        g2.setStroke(stroke);
        g.drawLine((i * relativoAncho) + (relativoAncho / 2), (j * relativoAlto) - (relativoAlto / 8), (i * relativoAncho) + (relativoAncho / 2), (j * relativoAlto) + (relativoAlto / 8));
        g2.setStroke(auxStroke);
    }

    private void dibujarAbajoIzquierda(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        char c = 8627;
        g.drawString(String.valueOf(c), (i * relativoAncho), (j * relativoAlto) + +(int) (relativoAncho));
        g.setFont(aux);
    }

    private void dibujarAbajoDerecha(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        char c = 8624;
        Graphics2D g2d = (Graphics2D) g;
        AffineTransform backup = g2d.getTransform();
        AffineTransform at = AffineTransform.getQuadrantRotateInstance(1);
        g2d.setTransform(at);
        g2d.drawString(String.valueOf(c), (j * relativoAlto) + (int) (relativoAncho * 0.5), -((i * relativoAncho) + (int) (relativoAncho * 0.5)));
        g2d.setTransform(backup);
        g.setFont(aux);
    }

    private void dibujarArribaDerecha(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        char c = 8624;
        g.drawString(String.valueOf(c), (i * relativoAncho) + (int) (relativoAncho * 0.5), (j * relativoAlto) + (int) (relativoAncho * 0.6));
        g.setFont(aux);
    }

    private void dibujarArribaIzquierda(int i, int j, Graphics g) {
        Font aux = g.getFont();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        char c = 8627;
        Graphics2D g2d = (Graphics2D) g;
        AffineTransform backup = g2d.getTransform();
        AffineTransform at = AffineTransform.getQuadrantRotateInstance(1);
        g2d.setTransform(at);
        g2d.drawString(String.valueOf(c), (j * relativoAlto) + (int) (relativoAncho * 0), -((i * relativoAncho) + (int) (relativoAncho * 0.1)));
        g2d.setTransform(backup);
        g.setFont(aux);
    }

    private void dibujarGuiaIzquierda(int i, int j, String guia, Graphics g) {
        Font auxFont = g.getFont();
        Font myFont = new Font("Arial", 1, 4);
        g.setFont(myFont);
        alinearGuiaIzquierda(guia, i, j, g);
        g.setFont(auxFont);
    }

    private void dibujarGuiaArriba(int i, int j, String guia, Graphics g) {
        Font auxFont = g.getFont();
        Font myFont = new Font("Arial", 1, 4);
        g.setFont(myFont);
        alinearGuiaArriba(guia, i, j, g);
        g.setFont(auxFont);
    }

    private void dibujarGuiaDerecha(int i, int j, String guia, Graphics g) {
        Font auxFont = g.getFont();
        Font myFont = new Font("Arial", 1, 4);
        g.setFont(myFont);
        alinearGuiaDerecha(guia, i, j, g);
        g.setFont(auxFont);
    }

    private void dibujarGuiaAbajo(int i, int j, String guia, Graphics g) {
        Font auxFont = g.getFont();
        Font myFont = new Font("Arial", 1, 4);
        g.setFont(myFont);
        alinearGuiaAbajo(guia, i, j, g);
        g.setFont(auxFont);
    }

    private void alinearGuiaArriba(String guia, int i, int j, Graphics g) {
        if (guia.length() <= 10) {
            guia = espacios(guia);
            int x = (i * relativoAncho) - (int) (relativoAncho * 0.9);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.8);
            g.drawString(guia, x, y);
        } else {
            String guia1 = guia.substring(0, 10);
            guia1 = espacios(guia1);
            int x = (i * relativoAncho) - (int) (relativoAncho * 0.9);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.8);
            g.drawString(guia1, x, y);
            String guia2 = guia.substring(10);
            guia2 = espacios(guia2);
            x = (i * relativoAncho) - (int) (relativoAncho * 0.9);
            y = (j * relativoAlto) - (int) (relativoAlto * 0.65);
            g.drawString(guia2, x, y);
        }
    }

    private void alinearGuiaIzquierda(String guia, int i, int j, Graphics g) {
        if (guia.length() <= 10) {
            guia = espacios(guia);
            Graphics2D g2d = (Graphics2D) g;
            AffineTransform backup = g2d.getTransform();
            AffineTransform at = AffineTransform.getQuadrantRotateInstance(3);
            g2d.setTransform(at);
            int x = (i * relativoAncho) - (int) (relativoAncho * 0.8);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.1);
            g2d.drawString(guia, -y, x);
            g2d.setTransform(backup);
        } else {
            String guia1 = guia.substring(0, 10);
            guia1 = espacios(guia1);
            Graphics2D g2d = (Graphics2D) g;
            AffineTransform backup = g2d.getTransform();
            AffineTransform at = AffineTransform.getQuadrantRotateInstance(3);
            g2d.setTransform(at);
            int x = (i * relativoAncho) - (int) (relativoAncho * 0.85);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.1);
            g2d.drawString(guia1, -y, x);
            String guia2 = guia.substring(10);
            guia2 = espacios(guia2);
            x = (i * relativoAncho) - (int) (relativoAncho * 0.85);
            y = (j * relativoAlto) - (int) (relativoAlto * 0.1);
            g2d.drawString(guia2, -y, x);
            g.drawString(guia2, x, y);
            g2d.setTransform(backup);
        }
    }

    private void alinearGuiaDerecha(String guia, int i, int j, Graphics g) {
        if (guia.length() <= 10) {
            guia = espacios(guia);
            Graphics2D g2d = (Graphics2D) g;
            AffineTransform backup = g2d.getTransform();
            AffineTransform at = AffineTransform.getQuadrantRotateInstance(1);
            g2d.setTransform(at);
            int x = (i * relativoAncho) - (int) (relativoAlto * 0.15);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.9);
            g2d.drawString(guia, y, -x);
            g2d.setTransform(backup);
        } else {
            String guia1 = guia.substring(0, 10);
            guia1 = espacios(guia1);
            Graphics2D g2d = (Graphics2D) g;
            AffineTransform backup = g2d.getTransform();
            AffineTransform at = AffineTransform.getQuadrantRotateInstance(1);
            g2d.setTransform(at);
            int x = (i * relativoAncho) - (int) (relativoAlto * 0.15);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.9);
            g2d.drawString(guia1, y, -x);
            String guia2 = guia.substring(10);
            guia2 = espacios(guia2);
            x = (i * relativoAncho) - (int) (relativoAlto * 0.30);
            y = (j * relativoAlto) - (int) (relativoAlto * 0.9);
            g2d.drawString(guia2, y, -x);
            g.drawString(guia2, x, y);
            g2d.setTransform(backup);
        }
    }

    private void alinearGuiaAbajo(String guia, int i, int j, Graphics g) {
        if (guia.length() <= 10) {
            guia = espacios(guia);
            int x = (i * relativoAncho) - (int) (relativoAncho * 0.9);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.05);
            g.drawString(guia, x, y);
        } else {
            String guia1 = guia.substring(0, 10);
            guia1 = espacios(guia1);
            int x = (i * relativoAncho) - (int) (relativoAncho * 0.9);
            int y = (j * relativoAlto) - (int) (relativoAlto * 0.15);
            g.drawString(guia1, x, y);
            String guia2 = guia.substring(10);
            guia2 = espacios(guia2);
            x = (i * relativoAncho) - (int) (relativoAncho * 0.9);
            y = (j * relativoAlto) - (int) (relativoAlto * 0.05);
            g.drawString(guia2, x, y);
        }
    }

    private String espacios(String guia) {
        int cantidad = 10 - guia.length();
        String result = "";
        if (cantidad > 1) {
            int i = 0;
            for (i = 0; i < (cantidad / 2); i++) {
                result += " ";
            }
            result += guia;
            int faltantes = cantidad - i;
            for (i = 0; i < faltantes; i++) {
                result += " ";
            }
        } else {
            result += (guia + " ");
        }
        return result;
    }

    private void dibujarVacio(Graphics g) {
        crucigrama = CruciUtils.inicializar(plantilla);
        if (crucigrama.length > 0) {
            int altoCrucigrama = crucigrama.length;
            int anchoCrucigrama = crucigrama[0].length;

            for (int i = 0; i < altoCrucigrama; i++) {
                for (int j = 0; j < anchoCrucigrama; j++) {
                    String letra = crucigrama[i][j];
                    if (letra.equals(".")) {
                        letra = "";
                    }
                    if (letra.equals("#")) {
                        letra = "#";
                    }
                    if (letra.equals("@")) {
                        letra = "@";
                    }
                    dibujarModelo(j, i, letra, g);
                }
            }
        }
    }

    private void generateBackground() {
        try {
            Crucigrama c = new Crucigrama(this, contenedor, plantilla, limpiar);
            if (c.existe()) {
                boolean ok = c.procesar();
                if (!ok) {
                    waitingDialog.setVisible(false);
                    return;
                }
                crucigrama = c.getCrucigrama();
                simbolos = c.getSimbolos();
                guias = c.getGuias();

//                System.out.println("###############################################################");
//                System.out.println("############            GUIAS           ######################");
//                System.out.println("###############################################################");
//                Iterator<Map.Entry<Posicion, List<Leyenda>>> iter = guias.entrySet().iterator();
//                while(iter.hasNext()){
//                    Map.Entry<Posicion, List<Leyenda>> next = iter.next();
//                    int x = 0;
//                    for (Leyenda ley : next.getValue()) {
//                        x++;
//                        System.out.println("[" + next.getKey().getX() + " - " +  next.getKey().getY() + "] = (" + x + ") " + ley.getLeyenda().getPalabra());
//                    }
//                }
            } else {
                JOptionPane.showMessageDialog(principal, "No exiten palabras disponibles.\nCargue un archivo de palabras desde el menú \nPalabras > Cargar archivo de palabras.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            waitingDialog.setVisible(false);
        }
    }

    private void generatePdf(String filePath) {
        try {
            Map parameterMap = new HashMap();
            pdfLetras(parameterMap);
            if (mostrarSimbolos) {
                pdfSimbolos(parameterMap);
            }

            //COMPILE FILE AND FILL PDF
//            String jrxml = "D:/plantilla.jrxml";
//            String jasper = "D:/plantilla.jasper";
//            JasperCompileManager.compileReportToFile(jrxml, jasper);
//            JasperReport jasperReport = JasperCompileManager.compileReport(jrxml);
//            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, new JREmptyDataSource());
            //FILL PDF WITH JASPER EXISTING FILE
            String jasper = "plantilla.jasper";
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper, parameterMap, new JREmptyDataSource());

            JasperExportManager.exportReportToPdfFile(jasperPrint, filePath);
            if (Desktop.isDesktopSupported()) {
                try {
                    boolean openedFile = false;
                    File createdFile = new File(filePath);
                    int count = 0;
                    while (!openedFile && count <= 5) {
                        if (createdFile.exists()) {
                            Desktop.getDesktop().open(createdFile);
                            openedFile = true;
                        } else {
                            count++;
                            Thread.sleep(3000);
                        }
                    }
                } catch (Exception ex) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        waitingDialog.setVisible(false);
    }
}
