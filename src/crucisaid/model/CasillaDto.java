package crucisaid.model;

public class CasillaDto {

    private String letra;
    private boolean finalIzquierda;
    private boolean finalArriba;
    private boolean finalDerecha;
    private boolean finalAbajo;
    private String ayudaIzquierda;
    private String ayudaArriba;
    private String ayudaDerecha;
    private String ayudaAbajo;
    private boolean cambioArribaIzquierda;
    private boolean cambioArribaDerecha;
    private boolean cambioAbajoDerecha;
    private boolean cambioAbajoIzquierda;
    private boolean inicioIzquierda;
    private boolean inicioArriba;
    private boolean inicioDerecha;
    private boolean inicioAbajo;

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public boolean isFinalIzquierda() {
        return finalIzquierda;
    }

    public void setFinalIzquierda(boolean finalIzquierda) {
        this.finalIzquierda = finalIzquierda;
    }

    public boolean isFinalArriba() {
        return finalArriba;
    }

    public void setFinalArriba(boolean finalArriba) {
        this.finalArriba = finalArriba;
    }

    public boolean isFinalDerecha() {
        return finalDerecha;
    }

    public void setFinalDerecha(boolean finalDerecha) {
        this.finalDerecha = finalDerecha;
    }

    public boolean isFinalAbajo() {
        return finalAbajo;
    }

    public void setFinalAbajo(boolean finalAbajo) {
        this.finalAbajo = finalAbajo;
    }

    public String getAyudaIzquierda() {
        return ayudaIzquierda;
    }

    public void setAyudaIzquierda(String ayudaIzquierda) {
        this.ayudaIzquierda = ayudaIzquierda;
    }

    public String getAyudaArriba() {
        return ayudaArriba;
    }

    public void setAyudaArriba(String ayudaArriba) {
        this.ayudaArriba = ayudaArriba;
    }

    public String getAyudaDerecha() {
        return ayudaDerecha;
    }

    public void setAyudaDerecha(String ayudaDerecha) {
        this.ayudaDerecha = ayudaDerecha;
    }

    public String getAyudaAbajo() {
        return ayudaAbajo;
    }

    public void setAyudaAbajo(String ayudaAbajo) {
        this.ayudaAbajo = ayudaAbajo;
    }

    public boolean isCambioArribaIzquierda() {
        return cambioArribaIzquierda;
    }

    public void setCambioArribaIzquierda(boolean cambioArribaIzquierda) {
        this.cambioArribaIzquierda = cambioArribaIzquierda;
    }

    public boolean isCambioArribaDerecha() {
        return cambioArribaDerecha;
    }

    public void setCambioArribaDerecha(boolean cambioArribaDerecha) {
        this.cambioArribaDerecha = cambioArribaDerecha;
    }

    public boolean isCambioAbajoDerecha() {
        return cambioAbajoDerecha;
    }

    public void setCambioAbajoDerecha(boolean cambioAbajoDerecha) {
        this.cambioAbajoDerecha = cambioAbajoDerecha;
    }

    public boolean isCambioAbajoIzquierda() {
        return cambioAbajoIzquierda;
    }

    public void setCambioAbajoIzquierda(boolean cambioAbajoIzquierda) {
        this.cambioAbajoIzquierda = cambioAbajoIzquierda;
    }

    public boolean isInicioIzquierda() {
        return inicioIzquierda;
    }

    public void setInicioIzquierda(boolean inicioIzquierda) {
        this.inicioIzquierda = inicioIzquierda;
    }

    public boolean isInicioArriba() {
        return inicioArriba;
    }

    public void setInicioArriba(boolean inicioArriba) {
        this.inicioArriba = inicioArriba;
    }

    public boolean isInicioDerecha() {
        return inicioDerecha;
    }

    public void setInicioDerecha(boolean inicioDerecha) {
        this.inicioDerecha = inicioDerecha;
    }

    public boolean isInicioAbajo() {
        return inicioAbajo;
    }

    public void setInicioAbajo(boolean inicioAbajo) {
        this.inicioAbajo = inicioAbajo;
    }

    
}
