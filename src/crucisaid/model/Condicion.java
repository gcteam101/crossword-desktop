package crucisaid.model;

import java.util.Map;

/**
 *
 * @author Carlos
 */
public class Condicion {

    private int longitud;
    private Posicion inicio;
    private Map<Integer, String> condiciones;

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public Posicion getInicio() {
        return inicio;
    }

    public void setInicio(Posicion inicio) {
        this.inicio = inicio;
    }

    public Map<Integer, String> getCondiciones() {
        return condiciones;
    }

    public void setCondiciones(Map<Integer, String> condiciones) {
        this.condiciones = condiciones;
    }

}
