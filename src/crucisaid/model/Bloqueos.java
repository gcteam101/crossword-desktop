package crucisaid.model;

/**
 *
 * Esta clase servirá para contener los bloqueos de un crucigrama.
 * Los bloqueos indican si la letra inicia, termina o es intermedia de la palabra.
 * 
 * @author Carlos
 */
public class Bloqueos {

    private boolean bloqueoIzquierda;
    private boolean bloqueoDerecha;
    private boolean bloqueoArriba;
    private boolean bloqueoAbajo;

    public Bloqueos(boolean bloqueoIzquierda, boolean bloqueoDerecha, boolean bloqueoArriba, boolean bloqueoAbajo) {
        this.bloqueoIzquierda = bloqueoIzquierda;
        this.bloqueoDerecha = bloqueoDerecha;
        this.bloqueoArriba = bloqueoArriba;
        this.bloqueoAbajo = bloqueoAbajo;
    }

    public Bloqueos() {
    }
    
    /**
     * @return the bloqueoIzquierda
     */
    public boolean isBloqueoIzquierda() {
        return bloqueoIzquierda;
    }

    /**
     * @param bloqueoIzquierda the bloqueoIzquierda to set
     */
    public void setBloqueoIzquierda(boolean bloqueoIzquierda) {
        this.bloqueoIzquierda = bloqueoIzquierda;
    }

    /**
     * @return the bloqueoDerecha
     */
    public boolean isBloqueoDerecha() {
        return bloqueoDerecha;
    }

    /**
     * @param bloqueoDerecha the bloqueoDerecha to set
     */
    public void setBloqueoDerecha(boolean bloqueoDerecha) {
        this.bloqueoDerecha = bloqueoDerecha;
    }

    /**
     * @return the bloqueoArriba
     */
    public boolean isBloqueoArriba() {
        return bloqueoArriba;
    }

    /**
     * @param bloqueoArriba the bloqueoArriba to set
     */
    public void setBloqueoArriba(boolean bloqueoArriba) {
        this.bloqueoArriba = bloqueoArriba;
    }

    /**
     * @return the bloqueoAbajo
     */
    public boolean isBloqueoAbajo() {
        return bloqueoAbajo;
    }

    /**
     * @param bloqueoAbajo the bloqueoAbajo to set
     */
    public void setBloqueoAbajo(boolean bloqueoAbajo) {
        this.bloqueoAbajo = bloqueoAbajo;
    }
}
