package crucisaid.model;

/**
 *
 * @author Carlos
 */
public class Palabra {

    private int id;
    private int cantidad;
    private String palabra;
    private String descripcion;

    public Palabra() {
    }

    public Palabra(int cantidad, String palabra, String descripcion) {
        this.cantidad = cantidad;
        this.palabra = palabra;
        this.descripcion = descripcion;
    }

    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the palabra
     */
    public String getPalabra() {
        return palabra;
    }

    /**
     * @param palabra the palabra to set
     */
    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Palabra{" + "cantidad=" + cantidad + ", palabra=" + palabra + ", descripcion=" + descripcion + '}';
    }
}