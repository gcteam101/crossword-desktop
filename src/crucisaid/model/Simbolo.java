package crucisaid.model;

public class Simbolo {

    private int idSimbolo;
    private int posicionDibujar;

    /**
     * @return the idSimbolo
     */
    public int getIdSimbolo() {
        return idSimbolo;
    }

    /**
     * @param idSimbolo the idSimbolo to set
     */
    public void setIdSimbolo(int idSimbolo) {
        this.idSimbolo = idSimbolo;
    }

    /**
     * @return the posicionDibujar
     */
    public int getPosicionDibujar() {
        return posicionDibujar;
    }

    /**
     * @param posicionDibujar the posicionDibujar to set
     */
    public void setPosicionDibujar(int posicionDibujar) {
        this.posicionDibujar = posicionDibujar;
    }

}
