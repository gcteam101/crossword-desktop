package crucisaid.model;

public class Leyenda {

    private int posicionDibujar;
    private Palabra leyenda;

    /**
     * @return the posicionDibujar
     */
    public int getPosicionDibujar() {
        return posicionDibujar;
    }

    /**
     * @param posicionDibujar the posicionDibujar to set
     */
    public void setPosicionDibujar(int posicionDibujar) {
        this.posicionDibujar = posicionDibujar;
    }

    /**
     * @return the leyenda
     */
    public Palabra getLeyenda() {
        return leyenda;
    }

    /**
     * @param leyenda the leyenda to set
     */
    public void setLeyenda(Palabra leyenda) {
        this.leyenda = leyenda;
    }

}
