package crucisaid.utils;

/**
 *
 * @author Carlos
 */
public class Constantes {

    public static final int DIRECCION_IZQUIERDA = 0;
    public static final int DIRECCION_DERECHA = 1;
    public static final int DIRECCION_ARRIBA = 2;
    public static final int DIRECCION_ABAJO = 3;

    public static final int DIBUJAR_ABAJ_IZQ = 0;
    public static final int DIBUJAR_ABAJ_DER = 1;
    public static final int DIBUJAR_ARRI_DER = 2;
    public static final int DIBUJAR_ARRI_IZQ = 3;
    public static final int DIBUJAR_ABAJO = 4;
    public static final int DIBUJAR_DERECHA = 5;
    public static final int DIBUJAR_ARRIBA = 6;
    public static final int DIBUJAR_IZQUIERDA = 7;

    public static final int SIMBOLO_ABAJ_IZQ = 0;
    public static final int SIMBOLO_ABAJ_DER = 1;
    public static final int SIMBOLO_ARRI_DER = 2;
    public static final int SIMBOLO_ARRI_IZQ = 3;
    public static final int SIMBOLO_FINAL = 4;
    public static final int SIMBOLO_INICIAL = 5;
    public static final int SIMBOLO_ESPACIO = 6;

    public static final int MODELO_1 = 1;
    public static final int MODELO_2 = 2;
    public static final int MODELO_3 = 3;
    public static final int MODELO_4 = 4;
    public static final int MODELO_5 = 5;
    public static final int MODELO_6 = 6;
    public static final int MODELO_7 = 7;
    public static final int MODELO_8 = 8;
    public static final int MODELO_9 = 9;
    public static final int MODELO_10 = 10;
    public static final int MODELO_11 = 11;
    public static final int MODELO_12 = 12;
    public static final int MODELO_13 = 13;
    public static final int MODELO_14 = 14;
    public static final int MODELO_15 = 15;
    public static final int MODELO_16 = 16;
    public static final int MODELO_17 = 17;
    public static final int MODELO_18 = 18;
    public static final int MODELO_19 = 19;
    public static final int MODELO_20 = 20;
}
