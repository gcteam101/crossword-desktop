package crucisaid.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.PropertyConfigurator;

public class Parametros {

    private static Properties props = new Properties();

    static {
        try {
            props.load(new FileInputStream("etc/config.properties"));  
        } catch (IOException ex) {
        }
        
        PropertyConfigurator.configure("etc/log4j.properties");
    }

    public static int matrixX = Integer.parseInt((String) props.get("crucigrama.ancho"));
    public static int matrixY = Integer.parseInt((String) props.get("crucigrama.alto"));

    public static String archivoPalabras = (String) props.get("archivo.palabras");
    public static int palabrasMaximo = Integer.parseInt((String) props.get("palabras.maximo"));

    public static String modelo1Inicios = (String) props.get("modelo1.inicios");
    public static String modelo1Imagenes = (String) props.get("modelo1.imagenes");
    public static String modelo1Rutas = (String) props.get("modelo1.rutas");

    public static String modelo2Inicios = (String) props.get("modelo2.inicios");
    public static String modelo2Imagenes = (String) props.get("modelo2.imagenes");
    public static String modelo2Rutas = (String) props.get("modelo2.rutas");

    public static String modelo3Inicios = (String) props.get("modelo3.inicios");
    public static String modelo3Imagenes = (String) props.get("modelo3.imagenes");
    public static String modelo3Rutas = (String) props.get("modelo3.rutas");

    public static String modelo4Inicios = (String) props.get("modelo4.inicios");
    public static String modelo4Imagenes = (String) props.get("modelo4.imagenes");
    public static String modelo4Rutas = (String) props.get("modelo4.rutas");

    public static String modelo5Inicios = (String) props.get("modelo5.inicios");
    public static String modelo5Imagenes = (String) props.get("modelo5.imagenes");
    public static String modelo5Rutas = (String) props.get("modelo5.rutas");

    public static String modelo6Inicios = (String) props.get("modelo6.inicios");
    public static String modelo6Imagenes = (String) props.get("modelo6.imagenes");
    public static String modelo6Rutas = (String) props.get("modelo6.rutas");

    public static String modelo7Inicios = (String) props.get("modelo7.inicios");
    public static String modelo7Imagenes = (String) props.get("modelo7.imagenes");
    public static String modelo7Rutas = (String) props.get("modelo7.rutas");

    public static String modelo8Inicios = (String) props.get("modelo8.inicios");
    public static String modelo8Imagenes = (String) props.get("modelo8.imagenes");
    public static String modelo8Rutas = (String) props.get("modelo8.rutas");

    public static String modelo9Inicios = (String) props.get("modelo9.inicios");
    public static String modelo9Imagenes = (String) props.get("modelo9.imagenes");
    public static String modelo9Rutas = (String) props.get("modelo9.rutas");

    public static String modelo10Inicios = (String) props.get("modelo10.inicios");
    public static String modelo10Imagenes = (String) props.get("modelo10.imagenes");
    public static String modelo10Rutas = (String) props.get("modelo10.rutas");

    public static String modelo11Inicios = (String) props.get("modelo11.inicios");
    public static String modelo11Imagenes = (String) props.get("modelo11.imagenes");
    public static String modelo11Rutas = (String) props.get("modelo11.rutas");

    public static String modelo12Inicios = (String) props.get("modelo12.inicios");
    public static String modelo12Imagenes = (String) props.get("modelo12.imagenes");
    public static String modelo12Rutas = (String) props.get("modelo12.rutas");

    public static String modelo13Inicios = (String) props.get("modelo13.inicios");
    public static String modelo13Imagenes = (String) props.get("modelo13.imagenes");
    public static String modelo13Rutas = (String) props.get("modelo13.rutas");

    public static String modelo14Inicios = (String) props.get("modelo14.inicios");
    public static String modelo14Imagenes = (String) props.get("modelo14.imagenes");
    public static String modelo14Rutas = (String) props.get("modelo14.rutas");

    public static String modelo15Inicios = (String) props.get("modelo15.inicios");
    public static String modelo15Imagenes = (String) props.get("modelo15.imagenes");
    public static String modelo15Rutas = (String) props.get("modelo15.rutas");

    public static String modelo16Inicios = (String) props.get("modelo16.inicios");
    public static String modelo16Imagenes = (String) props.get("modelo16.imagenes");
    public static String modelo16Rutas = (String) props.get("modelo16.rutas");

    public static String modelo17Inicios = (String) props.get("modelo17.inicios");
    public static String modelo17Imagenes = (String) props.get("modelo17.imagenes");
    public static String modelo17Rutas = (String) props.get("modelo17.rutas");

    public static String modelo18Inicios = (String) props.get("modelo18.inicios");
    public static String modelo18Imagenes = (String) props.get("modelo18.imagenes");
    public static String modelo18Rutas = (String) props.get("modelo18.rutas");

    public static String modelo19Inicios = (String) props.get("modelo19.inicios");
    public static String modelo19Imagenes = (String) props.get("modelo19.imagenes");
    public static String modelo19Rutas = (String) props.get("modelo19.rutas");

    public static String modelo20Inicios = (String) props.get("modelo20.inicios");
    public static String modelo20Imagenes = (String) props.get("modelo20.imagenes");
    public static String modelo20Rutas = (String) props.get("modelo20.rutas");

    public static String username = (String) props.get("username");
    public static String  password = (String) props.get("password");
}
