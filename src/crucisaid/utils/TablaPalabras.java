package crucisaid.utils;

import crucisaid.model.Palabra;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Carlos
 */
public class TablaPalabras {

    public static Object[][] leerPalabras(String buscar) throws Exception {
        try {
            Connection conn = DatabaseAccess.abrir();
            List<Palabra> lista = DatabaseAccess.lista(conn, buscar);
            DatabaseAccess.cerrar(conn);

            Object[][] data = new Object[lista.size()][4];
            for (int i = 0; i < lista.size(); i++) {
                Palabra palabra = lista.get(i);
                data[i][0] = palabra.getId();
                data[i][1] = palabra.getPalabra();
                data[i][2] = palabra.getDescripcion();
                data[i][3] = palabra.getCantidad();
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Object[0][0];
    }

    public static String guardar(Palabra p) {
        String error = "";
        try {
            Connection conn = DatabaseAccess.abrir();
            long id = DatabaseAccess.push(conn, p);
            error = id > 0 ? String.valueOf(id) : "Error guardando la palabra.";
            DatabaseAccess.cerrar(conn);
        } catch (Exception e) {
            error = "Error al guardar la palabra, intente luego: " + e.getMessage();
        }
        return error;
    }

    public static String eliminar(int id) {
        String error = "";
        try {
            Connection conn = DatabaseAccess.abrir();
            DatabaseAccess.eliminar(conn, id);
            DatabaseAccess.cerrar(conn);
        } catch (Exception e) {
            error = "Error al eliminar la palabra, intente luego: " + e.getMessage();
        }
        return error;
    }
}
