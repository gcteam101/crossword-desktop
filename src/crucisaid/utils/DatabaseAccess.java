package crucisaid.utils;

import crucisaid.model.Condicion;
import crucisaid.model.Palabra;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Carlos
 */
public class DatabaseAccess {

    private static final String DATABASE = "jdbc:sqlite:crucisaid.db";

    static {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException ex) {
        }
    }

    public static Connection abrir() throws Exception {
        Connection conn = DriverManager.getConnection(DATABASE);
        return conn;
    }

    public static void cerrar(Connection conn) throws Exception {
        if (conn != null && !conn.isClosed()) {
            conn.close();
        }
    }

    public static void crear(Connection conn) throws Exception {
        Statement stmt = conn.createStatement();
        String sql = "CREATE TABLE PALABRAS "
                + "(ID          INTEGER PRIMARY KEY AUTOINCREMENT,"
                + " PALABRA     TEXT NOT NULL, "
                + " CANTIDAD    INT NOT NULL, "
                + " USADA       INT NOT NULL, "
                + " DESCRIPCION TEXT NOT NULL)";
        stmt.executeUpdate(sql);
        stmt.close();
    }

    public static void destruir(Connection conn) {
        try {
            Statement stmt = conn.createStatement();
            String sql = "DROP TABLE IF EXISTS PALABRAS";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
        }
    }

    public static void usada(Connection conn, int id) throws Exception {
        String sql = "UPDATE PALABRAS "
                + "SET USADA = 1 "
                + "WHERE ID = ?;";

        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, id);
        pst.executeUpdate();
    }

    public static void eliminar(Connection conn, int id) throws Exception {
        String sql = "DELETE FROM PALABRAS WHERE ID = ?;";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, id);
        pst.executeUpdate();
    }

    public static void limpiar(Connection conn) throws Exception {
        String sql = "UPDATE PALABRAS SET USADA = 0;";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.executeUpdate();
    }

    public static long push(Connection conn, Palabra palabra) throws Exception {
        String sql = "INSERT INTO PALABRAS "
                + "(PALABRA, CANTIDAD, USADA, DESCRIPCION) "
                + "VALUES (?,?,0,?)";

        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, palabra.getPalabra());
        pst.setInt(2, palabra.getCantidad());
        pst.setString(3, palabra.getDescripcion());
        pst.executeUpdate();

        long id = 0;
        try (ResultSet generatedKeys = pst.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
        }
        return id;
    }

    public static Palabra pop(Connection conn, Condicion condicion) throws Exception {
        if (condicion.getLongitud() == 0) {
            System.out.println("Palabra no encontrada.");
            return null;
        }

        System.out.println("buscando Palabra de " + condicion.getLongitud() + " letras...");
        Palabra palabra = null;
        Map<Integer, String> map = condicion.getCondiciones();
        String query = "";
        for (int i = 0; i < condicion.getLongitud(); i++) {
            if (map.containsKey(i + 1)) {
                String letra = map.get(i + 1);
                query += (letra);
            } else {
                query += "_";
            }
        }//buscara el match exacto, hay que buscar con menos cantidad de letras si no pilla

        //String sql = "SELECT * FROM PALABRAS WHERE REPLACE(PALABRA, ' ', '') LIKE ? AND USADA = 0 ORDER BY CANTIDAD ASC;";
        String sql = "SELECT * FROM PALABRAS WHERE REPLACE(PALABRA, ' ', '') LIKE ? AND USADA = 0 ORDER BY RANDOM();";
//        String sql = "SELECT * FROM PALABRAS WHERE PALABRA LIKE ?;";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, query);
        ResultSet rs = pst.executeQuery();
        if (rs.next()) {
            palabra = new Palabra();
            int _id = rs.getInt("ID");
            String _palabra = rs.getString("PALABRA");
            int _cantidad = rs.getInt("CANTIDAD");
            String _descripcion = rs.getString("DESCRIPCION");
            palabra.setId(_id);
            palabra.setPalabra(_palabra.trim());
            palabra.setCantidad(_cantidad);
            palabra.setDescripcion(_descripcion);
            usada(conn, _id);
        } else {
            int newLongitud = condicion.getLongitud() - 1;
            System.out.println("Palabra no encontrada(" + query + "), intentando con " + newLongitud + " caracteres.");
            condicion.setLongitud(newLongitud);
            return pop(conn, condicion);
        }
        rs.close();
        pst.close();

        return palabra;
    }

    public static boolean existe(Connection conn) throws Exception {
        boolean respuesta = false;
        String sql = "SELECT * FROM PALABRAS;";
        PreparedStatement pst = conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        respuesta = rs.next();
        rs.close();
        pst.close();
        return respuesta;
    }

    public static List<Palabra> lista(Connection conn, String buscar) throws Exception {
        List<Palabra> respuesta = new ArrayList<>();
        String sql = "SELECT * FROM PALABRAS";
        buscar = buscar.toLowerCase();
        String sqlFind = buscar.isEmpty() ? ";" : " WHERE LOWER(PALABRA) LIKE ? OR LOWER(DESCRIPCION) LIKE ?;";
        sql += sqlFind;
        PreparedStatement pst = conn.prepareStatement(sql);
        if(!buscar.isEmpty()){
            pst.setString(1, "%" + buscar + "%");
            pst.setString(2, "%" + buscar + "%");
        }
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            Palabra palabra = new Palabra();
            int _id = rs.getInt("ID");
            String _palabra = rs.getString("PALABRA");
            int _cantidad = rs.getInt("CANTIDAD");
            String _descripcion = rs.getString("DESCRIPCION");
            palabra.setId(_id);
            palabra.setPalabra(_palabra);
            palabra.setCantidad(_cantidad);
            palabra.setDescripcion(_descripcion);
            respuesta.add(palabra);
        }
        rs.close();
        pst.close();
        return respuesta;
    }
}
